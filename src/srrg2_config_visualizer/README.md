# srrg2_config_visualizer

This package contains the tool for configuration visualization.
It allows you to load/save a configuration, visualize it and change
the parameters of each node.

## Prerequisites

This package requires:
* [srrg2_laser_slam_2d](https://gitlab.com/srrg-software/srrg2_laser_slam_2d)
* [srrg2_proslam](https://gitlab.com/srrg-software/srrg2_proslam)
* ... all their dependencies

External requirements:
* [Eigen3](http://eigen.tuxfamily.org/index.php?title=Main_Page)
* [GLUT](http://freeglut.sourceforge.net/docs/install.php)
* [glfw3](https://github.com/glfw/glfw.git)
