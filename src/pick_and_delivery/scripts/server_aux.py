from cmath import sqrt
from Queue import PriorityQueue
from copy import deepcopy


class node:
    def __init__(self, index, dist):
        self.index = index  # indice nella OccupancyGrid
        self.dist = dist  # distanza dalla sorgente
    def __cmp__(self, other):
        return self.dist > other.dist  # confronto dei nodi


def index(i, j, cols):
    return (i * cols) + j


def distance(index1, index2, cols):
    i1 = int(index1 / cols)
    i2 = int(index2 / cols)
    j1 = index1 - (i1 * cols)
    j2 = index2 - (i2 * cols)
    return sqrt(pow(i1 - i2, 2) + pow(j1 - j2, 2)).real


def get_neighbours(n, map, visited, queued, pq):
    direction_i = [-1, -1, -1, 0, 0, 1, 1, 1]
    direction_j = [-1, 0, 1, -1, 1, -1, 0, 1]
    
    rows = map.info.height  # altezza della mappa
    cols = map.info.width  # larghezza della mappa
    
    this_i = int(n.index / cols)
    this_j = n.index - (this_i*cols)
    

    
    for k in range(len(direction_i)):
        i = this_i + direction_i[k]
        j = this_j + direction_j[k]
        
        # controllo per verificare che la nuova cella rientri nella matrice
        if i < 0 or j < 0 or i >= rows or j >= cols:
            continue
        
        new_index = index(i,j, cols)
        if visited[new_index]:
            continue
        if queued[new_index]:
            continue
        if map.data[new_index] is not 0:
            continue
        
        pq.put(node(new_index, distance(n.index, new_index, cols) + n.dist))
        queued[new_index] = True


def dijkstra(map, source_coordinate, destination_coordinates):
    height = map.info.height  # altezza della mappa
    width = map.info.width  # larghezza della mappa
    size = width * height  # dimensione complessiva della mappa
    visited = [False] * size
    queued = [False] * size
    result = []
    destination = []

    # se w=1 occorre fare degli swap
    if map.info.origin.orientation.w == 1:
        app = source_coordinate[0]
        source_coordinate[0] = source_coordinate[1]
        source_coordinate[1] = app
        
        for coords in destination_coordinates:
            app = coords[0]
            coords[0] = coords[1]
            coords[1] = app
    
    x = int((source_coordinate[0] - map.info.origin.position.x) / map.info.resolution)  # conversione dalle coordinate della mappa agli indici della matrice
    y = int((source_coordinate[1] - map.info.origin.position.y) / map.info.resolution)  # conversione dalle coordinate della mappa agli indici della matrice
    source_cell = [x, y]
    source = index(source_cell[0], source_cell[1], width)
    
    for coords in destination_coordinates:
        x = int((coords[0] - map.info.origin.position.x) / map.info.resolution)  # conversione dalle coordinate della mappa agli indici della matrice
        y = int((coords[1] - map.info.origin.position.y) / map.info.resolution)  # conversione dalle coordinate della mappa agli indici della matrice
        destination.append(index(x, y, width))  # aggiunta dell'indice alla lista delle destinazioni
        result.append(0)  # aggiunta una nuova entry per ogni destinazione

    pq = PriorityQueue()
    pq.put(node(source, 0))
    destination_copy = list(destination)
    while not pq.empty():
        if not destination:
            return result
        this_node = pq.get()
        if this_node.index in destination:
            result[destination_copy.index(this_node.index)] = round(this_node.dist * map.info.resolution, 3)
            destination.remove(this_node.index)

        visited[this_node.index] = True
        get_neighbours(this_node, map, visited, queued, pq)