#!/usr/bin/env python

import rospy
import threading
import server_aux
import signal
import random

from socket import socket, AF_INET, SOCK_STREAM, SOL_SOCKET, SO_REUSEADDR
from geometry_msgs.msg import PoseStamped, PoseWithCovarianceStamped
from tf2_msgs.msg import TFMessage
from tf2_ros import TransformListener, Buffer
from math import sqrt
from rospkg import RosPack
from nav_msgs.msg import OccupancyGrid
from ortools.constraint_solver import routing_enums_pb2, pywrapcp
from sys import maxsize
from copy import deepcopy


package_path = RosPack().get_path('pick_and_delivery')

n_robot = 2  # numero di robot presenti nella simulazione
robot_initial_pose = [[49.495300293, 11.6122970581, 0, 0, 0, 0.999974491603, 0.00714255855827], # robot_0 [p.x, p.y, p.z, o.x, o.y, o.z, o.w]
                      [53.4049263, 11.6979093552, 0, 0, 0, -0.00423708500993, 0.999991023515]]  # robot_1 [p.x, p.y, p.z, o.x, o.y, o.z, o.w]

current_position = []  # posizione di ogni robot all'interno della mappa
goal_target = []  # lista dei target
goal_status = []  # stati di goal_status: -1->disponibile; 0->cruising; 1->destinazione; 2->fallito
goal_pub = []  # lista dei publisher dei robot per i goal
ultimate_goal = []  # lista dell'ultimo utente che ogni robot deve visitare
initialpose_pub = []  # lista di tutti i publisher per l'initialpose

tfBuffer = Buffer()  # dichiarazione tfBuffer come variabile di tibo tf2_ros.Buffer
server_map = []  # dati e metadati della mappa

seq = 1
t_timeout = 60  # numero di secondi per dare una risposta al server quando si sta bloccando una risorsa
t_delivery = 900  # numero di secondi a disposizione per arrivare a destinazione da quando viene dato un goal
timer_event_ids = []

users_logged = {}  # dizionario con chiave il nome utente. user_logged["-"] = [indice, coordinate, distanze, socket, disponibilita']
delivery_requests = []  # lista delle richieste. ogni elemento e' composto solo da mittente e destinatario, #### potrebbe essere interessante inserire una coda di priorita'
robot_queue = []  # coda di obiettivi dei robot

test = True


def handler(signum, frame):
    exit(0)


def position_callback(tfm):
    global current_position
    
    for i in range(n_robot):
        transform_ok = tfBuffer.can_transform("map", "robot_" + str(i) + "/base_link", rospy.Time(0))
        if transform_ok:
            transform_stamped = tfBuffer.lookup_transform("map", "robot_" + str(i) + "/base_link", rospy.Time(0))
            current_position[i][0] = transform_stamped.transform.translation.x
            current_position[i][1] = transform_stamped.transform.translation.y
            current_position[i][2] = transform_stamped.transform.rotation.z
            current_position[i][3] = transform_stamped.transform.rotation.w


def map_callback(grid):
    global server_map
    server_map = grid
    return

    
class NavigationCheck:
    def __init__(self, index):
        self.i = index
        rospy.Timer(rospy.Duration(0.5), self.navigation_callback)  # controllo della posizione ogni 0.5 secondi

    def navigation_callback(self, timer):
        global goal_status
        global current_position
        global goal_target
        
        if goal_status[self.i] == 0:
            distance_remaining = sqrt(pow(current_position[self.i][0] - goal_target[self.i][0], 2) + pow(current_position[self.i][1] - goal_target[self.i][1], 2))
            
            if distance_remaining < 1:
                goal_status[self.i] = 1  # aggiornamento status navigazione di tracking


def localization():
    global seq
    global n_robot
    global initialpose_pub
    
    initial_pose = PoseWithCovarianceStamped()
    initial_pose.header.frame_id = "map"
    initial_pose.header.stamp = rospy.Time.now()
    initial_pose.pose.covariance = [0.25, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.25, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.06853892326654787]

    for i in range(n_robot):
        initial_pose.header.seq = seq
        seq += 1
        
        initial_pose.pose.pose.position.x = robot_initial_pose[i][0]
        initial_pose.pose.pose.position.y = robot_initial_pose[i][1]
        initial_pose.pose.pose.position.z = robot_initial_pose[i][2]
        
        initial_pose.pose.pose.orientation.x = robot_initial_pose[i][3]
        initial_pose.pose.pose.orientation.y = robot_initial_pose[i][4]
        initial_pose.pose.pose.orientation.z = robot_initial_pose[i][5]
        initial_pose.pose.pose.orientation.w = robot_initial_pose[i][6]
        
        initialpose_pub[i].publish(initial_pose)


def init():
    global n_robot
    global initialpose_pub
    global goal_pub
    global goal_status
    global goal_target
    global robot_queue
    global current_position
    global ultimate_goal
    global timer_event_ids
    
    signal.signal(signal.SIGINT, handler)
    rospy.init_node('pick_and_delivery')  # inizializzazione del nodo ros
    
    TransformListener(tfBuffer)
    rospy.Subscriber("tf", TFMessage, position_callback)  # callback per tracciare i robot sulla mappa
    rospy.Subscriber("/map", OccupancyGrid, map_callback)  # callback per acquisire la mappa
    
    #timer_event_ids.append(0)  # in prima posizione viene inserito l'ID progressivo dei timer
    for i in range(n_robot):
        initialpose_pub.append(rospy.Publisher("/robot_"+ str(i) + "/initialpose", PoseWithCovarianceStamped, queue_size=10))  # creazione di un publisher initialpose per ogni robot
        goal_pub.append(rospy.Publisher("/robot_"+ str(i) + "/move_base_simple/goal", PoseStamped, queue_size=10))  # creazione di un publisher move_base_simple per ogni robot
        
        goal_target.append([0, 0])  # assegnazione del target 0 per ogni robot
        goal_status.append(-1)  # assegnazione status -1 per ogni robot
        ultimate_goal.append("")  # ultimo goal impostato a nullo
        current_position.append([0, 0, 0, 0])  # creazione di 2 oggetti tracciare la localizzazione dei robot
        robot_queue.append([])  # creazione di una coda per ogni robot
        timer_event_ids.append(0)  # ad ogni evento per ogni robot viene assegnato un id diverso
        NavigationCheck(i)
    
    rospy.sleep(1)  # aspetta che ogni publisher abbia i suoi listener
    rospy.Rate(10) # 10hz
    yn = raw_input("Vuoi usare la prima localizzazione dei robot? [s/n]\n")
    if yn == "s":
        localization()  # imposta la initial pose del robot
        rospy.sleep(0.2)


def distance_update(new_user_coords):
    global server_map
    
    # tentativo di apertura del file. Se non esiste allora l'utente e' il primo che si registra e avra' distanza da se stesso uguale a 0
    try:
        distance_file = open(package_path + '/scripts/distance_matrix.txt', 'r')
    except:
        distance_file = open(package_path + '/scripts/distance_matrix.txt', 'w')
        distance_file.write("0\n")
        return
    
    # lettura del file per l'acquisizione delle distanze gia' calcolate
    distance_matrix = []
    for line in distance_file:
       distance_matrix.append(line[:-1])
    distance_file.close()
    
    # lettura delle coordinate di tutti gli utenti
    users_file = open(package_path + '/scripts/users.txt', 'r')
    users_coords = []
    users_file.readline()  # lettura a vuoto di username
    line = users_file.readline()  # lettura di password solo per far iniziare in ciclo
    while line:
        line = users_file.readline()
        app = line.split(" ")
        try:
            users_coords.append([float(app[1]), float(app[3][:-1])])
        except:
            break
        users_file.readline()  # lettura a vuoto di riga vuota
        users_file.readline()  # lettura a vuoto di username
        users_file.readline()  # lettura a vuoto di password
    users_file.close()
    
    dist = server_aux.dijkstra(server_map, deepcopy(new_user_coords), deepcopy(users_coords))  # calcolo della distanza dal nuovo utente a tutti gli altri gia' registrati
    length = len(dist)
    distance_matrix.append("")  # aggiunta di una nuova riga relativa al nuovo utente registrato
    
    # calcolo distanza dal nuovo utente a tutti gli altri gia' registrati
    for i in range(length):
        distance_matrix[i] += "," + str(dist[i]) + "\n"
        distance_matrix[length] += str(dist[i]) + ","
    distance_matrix[length] += "0\n"  # rimozione virgola finale e aggiunta ritorno a capo
    
    # scrittura del risultato in un file
    distance_file = open(package_path + '/scripts/distance_matrix.txt', 'w')
    distance_file.writelines(distance_matrix)
    distance_file.close()


def registration(connection_socket):
    global users_logged
    
    #convalidazione username
    while not rospy.is_shutdown():
        usr_recv = str(connection_socket.recv(1024))  # ricezione username dal socket
        
        # controllo connessione attiva del client
        if usr_recv == "":
            return None

        users_file = open(package_path + '/scripts/users.txt', 'r')
        line = users_file.readline()
        i = 0
        while line:
            usr = line.split()[1]
            if usr == usr_recv:
                connection_socket.send("2")  # invio "2" per comunicare username gia' esistente
                break
            for i in range(3):
                users_file.readline()
            line = users_file.readline()
            i += 1
            
        
        if not line:
            connection_socket.send("1")  # invio "1" per comunicare username disponibile
            break   

        users_file.close()

    # acquisizione password e coordinate
    pwd_recv = str(connection_socket.recv(1024))  # ricezione password dal socket
    x = str(connection_socket.recv(1024))  # ricezione coordinata X dal socket
    y = str(connection_socket.recv(1024))  # ricezione coordinata Y dal socket

    # controllo connessione attiva del client
    if y == "":
        return None

    # aggiornamento file delle distanze
    distance_update([float(x), float(y)])
    # aggiornamento file degli utenti registrati
    users_file = open(package_path + '/scripts/users.txt', 'a')
    users_file.write("username: " + usr_recv + "\n")  # memorizzazione username
    users_file.write("password: " + pwd_recv + "\n")  # memorizzazione password
    users_file.write("x: " + x + " y: " + y + "\n")  # memorizzazione coordinate
    users_file.write("\n")
    users_file.close()
    
    connection_socket.send("1")  # invio "1" per comunicare registrazione completata

    # tentativo di apertura del file. Se non esiste allora l'utente e' il primo che si registra e avra' distanza da se stesso uguale a 0
    distances = None
    try:
        distance_file = open(package_path + '/scripts/distance_matrix.txt', 'r')
        for j in range(i):
            distance_file.readline()
        # acquisizione dati dal file delle distanze
        distances = distance_file.readline().split(",")
        distances[len(distances)-1] = distances[len(distances)-1][:-1]
        distances = [float(j) for j in distances]
    except:
        distances = [0]

    coords = [float(x), float(y)]
    users_logged[usr_recv] = [i, coords, distances, connection_socket, True]
    
    return usr_recv


def login(connection_socket):
    global users_logged
    
    try:
        users_file = open(package_path + '/scripts/users.txt', 'r')
    except:
        connection_socket.send("2")  # invio "2" per comunicare mancato login
        return None

    usr_recv = str(connection_socket.recv(1024))  # ricezione username dal socket
    pwd_recv = str(connection_socket.recv(1024))  # ricezione password dal socket

    i = -1
    while not rospy.is_shutdown():
        try:
            i += 1
            usr_read = users_file.readline().split()[1]  # acquisizione username dal file
            pwd_read = users_file.readline().split()[1]  # acquisizione password dal file
            if usr_recv == usr_read and pwd_recv == pwd_read:
                
                if usr_recv in users_logged:
                    connection_socket.send("3")  # invio "3" per comunicare che esiste gia' un utente online con lo stesso nome
                    return
                
                connection_socket.send("1")  # invio "1" per comunicare confermato login
                
                # lettura coordinate dal file
                row = users_file.readline().split()
                x = float(row[1])
                y = float(row[3])
                coords = [x, y]
                
                # acquisizione dati dal file delle distanze
                distance_file = open(package_path + '/scripts/distance_matrix.txt', 'r')
                for j in range(i):
                    distance_file.readline()
                distances = distance_file.readline().split(",")
                distances[len(distances)-1] = distances[len(distances)-1][:-1]
                distances = [float(j) for j in distances]
                
                users_logged[usr_recv] = [i, coords, distances, connection_socket, True]
                
                return usr_recv
            
            # lettura a vuoto delle coordinate che non interessano
            users_file.readline()
            users_file.readline()
        except Exception:
            connection_socket.send("2")  # invio "2" per comunicare mancato login
            return None


def delivery_acceptance(username):
    global delivery_requests
    global users_logged

    users_logged[username][4] = False  # l'utente viene visto come occupato finche' non completa questa attivita
    connection_socket = users_logged[username][3]  # acquisizione socket dell'utente
    connection_socket.recv(1024)  # ricezione per sincronizzazione con client
    
    usernames = users_logged.keys()  # acquisizione di tutti gli utenti registrati
    usernames.remove(username)  # rimozione dell'utente stesso dalla lista dei possibili destinatari
    
    for user in usernames:
        connection_socket.send(user)  # invio di 1 nome utente
        connection_socket.recv(1024)  # ricezione per sincronizzare con client
    connection_socket.send("1 1")  # invio segnale di fine
    
    socket_data = connection_socket.recv(1024)  # ricezione utente bersaglio
    if socket_data == "1 1":  # il client ha annullato l'invio del pacco
        users_logged[username][4] = True  # l'utente torna disponibile
        return
    
    for request in delivery_requests:
        if request == [username, socket_data]:
            connection_socket.send("2")  # invio sengale 2 per rifiutare la richiesta di spedizione
            users_logged[username][4] = True  # l'utente torna disponibile
            return
    
    connection_socket.send("1")
    delivery_requests.append([username, socket_data])  # aggiunta alla lista della richiesta di consegna
    
    users_logged[username][4] = True  # l'utente torna disponibile


class NavigationTimer:  # "override" di timer per considerare la target position
    def __init__(self, index):
        global t_delivery
        global timer_event_ids
        
        timer_event_ids[index] = timer_event_ids[index] + 1
        self.i = index
        self.timer_id = timer_event_ids[index]
        rospy.Timer(rospy.Duration(t_delivery), self.timer_CallBack, oneshot=True)  # vero avvio timer di consegna


    def timer_CallBack(self, timer):
        global seq
        global goal_status
        global ultimate_goal
        global timer_event_ids

        # se si attiva un evento precedente va assolutamente ignorato
        if self.timer_id is not timer_event_ids[self.i]:
            return
        
        # se il robot non e' in navigazione l'evento non deve fare nulla        
        if (goal_status[self.i] is not 0):
            return

        print("TIMEOUT. L'obiettivo non puo' essere raggiunto")
        
        transform_ok = tfBuffer.can_transform("map", "robot_" + str(self.i) + "/base_link", rospy.Time(0))
        while not transform_ok:
            transform_ok = tfBuffer.can_transform("map", "robot_" + str(self.i) + "/base_link", rospy.Time(0))
        
        transform_stamped = tfBuffer.lookup_transform("map", "robot_" + str(self.i) + "/base_link", rospy.Time(0))
        
        new_goal_msg = PoseStamped()

        new_goal_msg.header.frame_id = "map"
        new_goal_msg.header.stamp = rospy.Time.now()
        new_goal_msg.header.seq = seq
        seq += 1

        new_goal_msg.pose.position.x = transform_stamped.transform.translation.x
        new_goal_msg.pose.position.y = transform_stamped.transform.translation.y
        new_goal_msg.pose.position.z = 0

        new_goal_msg.pose.orientation.x = 0
        new_goal_msg.pose.orientation.y = 0
        new_goal_msg.pose.orientation.z = 0
        new_goal_msg.pose.orientation.w = 1
        
        # il goal pubblicato e' sulla stessa posizione del robot cosi' si ferma se si trova in movimento
        goal_pub[self.i].publish(new_goal_msg)
        ultimate_goal[self.i] = ""  # ultimo goal impostato a nulla per tenere in considerazione che il robot non e' da nessun utente
        goal_status[self.i] = 2  # eventuale notifica di irraggiungibilita'


def set_goal(coords, index_robot):
    global seq
    global goal_target

    new_goal_msg = PoseStamped()  # dichiarazione new_goal_msg come variabile di tipo PoseStamped
    
    # settaggio header
    new_goal_msg.header.frame_id = "map"
    new_goal_msg.header.stamp = rospy.Time.now()
    new_goal_msg.header.seq = seq
    seq += 1    

    # settaggio posizione bersaglio
    new_goal_msg.pose.position.x = coords[0]
    new_goal_msg.pose.position.y = coords[1]
    new_goal_msg.pose.position.z = 0

    # settaggio orientamento bersaglio (w=1 per comodita')
    new_goal_msg.pose.orientation.x = 0
    new_goal_msg.pose.orientation.y = 0
    new_goal_msg.pose.orientation.z = 0
    new_goal_msg.pose.orientation.w = 1

    # aggiornamento variabili di tracking
    goal_target[index_robot][0] = float(new_goal_msg.pose.position.x)
    goal_target[index_robot][1] = float(new_goal_msg.pose.position.y)
    goal_status[index_robot] = 0  # goal_status impostato in cruising
    
    goal_pub[index_robot].publish(new_goal_msg)  # pubblicazione messaggio goal
    NavigationTimer(int(index_robot))  # avvio timer di consegna


def to_sender(goal, index_robot):
    global users_logged
    global goal_status
    global test
    
    set_goal(users_logged[goal[0]][1], index_robot)
    while goal_status[index_robot] == 0:
        continue
    
    if goal_status[index_robot] == 2:
        print("Il robot_" + str(index_robot) + " non e' riuscito a raggiungere " + goal[0] + " per ricevere il pacco")
        print("La richiesta di spedizione di " + goal[0] + " e' stata annullata")
        try:
            users_logged[goal[0]][3].send("3"+goal[1])  # invio del segnale per comunicare quale richiesta e' stata annullata
        except:
            pass
        return False
    
    print("Il robot_" + str(index_robot) + " ha raggiunto " + goal[0])
    
    # in fase di testing o dimostrazione viene saltata la comunicazione con il client e il pacco risulta automaticamente accettato
    if test:
        print("Il robot_" + str(index_robot) + " ha preso in custodia il pacco di " + goal[0])
        return True
    
    # dialogo con il client per il caricamento del pacco 
    sentence = ""
    try:
        # se il client e' occupato si attende finche' non si libera
        while not users_logged[goal[0]][4]:
            continue
        users_logged[goal[0]][4] = False  # la disponibilita' del client viene interrotta
        users_logged[goal[0]][3].send("2")  # invio del segnale per comunicare l'arrivo e la richiesta di caricamento del pacco
        rospy.sleep(0.1)  # assicurazione per evitare che questo processo legga una risposta che non gli compete
        users_logged[goal[0]][3].settimeout(t_timeout)  # per evitare che il client occupi per un tempo indefinito un robot
        sentence = users_logged[goal[0]][3].recv(1024)
        users_logged[goal[0]][3].settimeout(None)  # rimozione vincolo temporale per comunicazione con il server
        users_logged[goal[0]][4] = True  # la disponibilita' del client viene ripristinata
    except:
        print("L'utente " + goal[0] + " non puo' caricare il pacco. Richiesta di spedizione annullata")
        users_logged[goal[0]][4] = True  # la disponibilita' del client viene ripristinata
        users_logged[goal[0]][3].close()
        return False
    if sentence == "1":
        print("Il robot_" + str(index_robot) + " ha preso in custodia il pacco di " + goal[0])
        return True
    elif sentence == "2":
        print("L'utente " + goal[0] + " ha annullato la richiesta di spedizione")
        return False


def back_to_sender(sender, index_robot):
    global goal_status
    global users_logged
    
    print("Ritorno del pacco al mittente")
    try:
        set_goal(users_logged[sender][1], index_robot)
    except:
        print("L'utente " + sender + " si e' disconnesso e il robot_" + str(index_robot) + " si e' sbarazzato del pacchetto")
        return False
    
    # attesa che il robot giunga ad una conclusione    
    while goal_status[index_robot] == 0:
        continue
    
    if goal_status[index_robot] == 1:
        print("Il robot_" + str(index_robot) + " ha riconsegnato forzatamente il pacchetto a " + sender)
        return True
    elif goal_status[index_robot] == 2:
        print("Il robot_" + str(index_robot) + " si e' sbarazzato del pacco")
        return False


def to_receiver(goal, index_robot):
    global users_logged
    global goal_status
    global ultimate_goal
    global test
    
    set_goal(users_logged[goal[1]][1], index_robot)
    while goal_status[index_robot] == 0:
        continue
    
    if goal_status[index_robot] == 2:
        print("Il robot_" + str(index_robot) + " non e' riuscito a raggiungere " + goal[1] + " per consegnare il pacco")
        ultimate_goal[index_robot] = goal[0]  # ultimo goal impostato al valore del receiver ovvero dove si fermera' il robot
        flag = back_to_sender(goal[0], index_robot)
        if not flag:
            ultimate_goal[index_robot] = ""  # non c'e' nessun ultimo goal
        return
    
    print("Il robot_" + str(index_robot) + " ha raggiunto " + goal[1])
    
    if test:
        print("Il robot_" + str(index_robot) + " ha consegnato il pacco a " + goal[1])
        return
    
    try:
        # se il client e' occupato si attende finche' non si libera
        while not users_logged[goal[1]][4]:
            continue
        users_logged[goal[1]][4] = False  # la disponibilita' del client viene interrotta
        users_logged[goal[1]][3].send("4")  # invio del segnale per comunicare la richiesta di consegna del pacco
        rospy.sleep(0.1)  # assicurazione per evitare che questo processo legga una risposta che non gli compete
        users_logged[goal[1]][3].settimeout(t_timeout)  # per evitare che il client occupi per un tempo indefinito un robot
        sentence = users_logged[goal[1]][3].recv(1024)
        users_logged[goal[1]][3].settimeout(None)  # rimozione vincolo temporale per comunicazione con il server
        users_logged[goal[1]][4] = True  # la disponibilita' del client viene ripristinata
        
        if sentence == "1":
            print("Consegna avvenuta con successo dall'utente " + goal[0] + " all'utente " + goal[1] + " per merito di robot_" + str(index_robot))
            return
        elif sentence == "2":
            print("Il pacco e' stato rifiutato dall'utente " + goal[1])
            ultimate_goal[index_robot] = goal[0]  # ultimo goal impostato al valore del receiver ovvero dove si fermera' il robot
            flag = back_to_sender(goal[0], index_robot)
            if not flag:
                ultimate_goal[index_robot] = ""  # non c'e' nessun ultimo goal
            return
        elif sentence == "":
            print("L'utente " + goal[1] + " ha chiuso la connessione. Il pacco verra' rispedito al mittente")
            ultimate_goal[index_robot] = goal[0]  # ultimo goal impostato al valore del receiver ovvero dove si fermera' il robot
            flag = back_to_sender(goal[0], index_robot)
            if not flag:
                ultimate_goal[index_robot] = ""  # non c'e' nessun ultimo goal
            return
    except:
        print("La connessione con " + goal[1] + " e' scaduta. Il pacco verra' rispedito al mittente")
        users_logged[goal[1]][4] = True  # la disponibilita' del client viene ripristinata
        ultimate_goal[index_robot] = goal[0]  # ultimo goal impostato al valore del receiver ovvero dove si fermera' il robot
        flag = back_to_sender(goal[0], index_robot)
        if not flag:
            ultimate_goal[index_robot] = ""  # non c'e' nessun ultimo goal
        return


def robot_dispatch(index_robot):
    global delivery_requests
    global robot_queue
    
    while not rospy.is_shutdown():
        # se non ci sono richieste in coda si attende finche' non arrivano
        if len(robot_queue[index_robot]) == 0:
            continue
                
        request = robot_queue[index_robot].pop(0)  # estrazione del primo elemento in coda
        delivery_requests.remove(request)  # quando la richiesta viene presa in incarico viene rimossa dalla lista
        ultimate_goal[index_robot] = request[1]  # ultimo goal impostato al valore del receiver ovvero dove si fermera' il robot
        print("\nIl robot_" + str(index_robot) + " ha preso in incarico la richiesta " + request[0] + "->" + request[1])
        
        flag = to_sender(request, index_robot)  # viaggio verso il mittente
        if not flag:
            print("Richiesta annullata. Se disponibile si procedera' alla prossima.\n")
            ultimate_goal[index_robot] = ""
            continue
        
        to_receiver(request, index_robot)  # viaggio verso il destinatario


def queue_cleaning(username):
    global delivery_requests
    global robot_queue
    
    for i in range(n_robot):
        length = len(robot_queue[i])
        j = 0
        while j < length:
            request = robot_queue[i][j]
            # rimozione delle richieste che non possono essere siddisfatte
            if request[0] == username or request[1] == username:
                print("A causa della disconnessione di " + username + " la richiesta " + request[0] + "->" + request[1] + " non puo' essere soddisfatta pertanto verra' rimossa.")
                robot_queue[i].remove(request)  # rimozione dalla coda singola
                delivery_requests.remove(request)  # rimozione dalla coda globale
                length -= 1
            else:
                j += 1


def server_service(connection_socket):
    global users_logged
    
    sentence = ""
    username = None

    # modulo per registrazione e login
    try:
        while username is None:
            sentence = str(connection_socket.recv(1024))
            if sentence == "1":
                username = login(connection_socket)
                
            elif sentence == "2":
                username = registration(connection_socket)
                
            else:
                return  # probabilmente la connessione e' stata chiusa
        
        print("L'utente " + username + " ha effettuato con successo il login")
    except:
        return
    
    # modulo per soddisfare le richieste del client
    try:
        while not rospy.is_shutdown():
            sentence = str(connection_socket.recv(1024))

            # richista di invio pacco da parte del client
            if sentence == "1":
                connection_socket.send("1")  # invio conferma accettazione richiesta
                delivery_acceptance(username)            
            
            # richiesta di logout da parte del client
            elif sentence == "2" or sentence == "":
                raise Exception()
            
            # il server smette di ascoltare le richieste del client finche' un robot non lo raggiunge
            elif sentence == "3":
                while not users_logged[username][4]:
                    continue
    except:
        users_logged.pop(username)  # rimozione dell'utente dalla lista dei connessi
        print("Utente " + username + " disconnesso")
        connection_socket.close()
        queue_cleaning(username)
        return


def get_distance():  # da modificare per vedere il prossimo obiettivo dei robot
    global current_position
    global users_logged
    global n_robot
    global server_map
    global ultimate_goal
    
    # creazione dizionaro risultato e aggiunta degli elementi per non andare in errore
    res = {}
    for usr in users_logged:
        res[usr] = []
    
    for i in range(n_robot):
        # se il robot e' fermo lontano da ogni utente deve calcolare con dijksta la distanza da ogni utente
        if ultimate_goal[i] == "":
            destination_coords = []
            for usr in users_logged:
                destination_coords.append(users_logged[usr][1])
            dist = server_aux.dijkstra(server_map, deepcopy(current_position[i]), deepcopy(destination_coords))
            j = 0
            for usr in users_logged:
                res[usr].append(dist[j])
                j += 1
        # se il robot e' sulla posizione di un utente oppure ci deve arrivare si puo' considerare quella locazione per calcoalre le distanze con gli altri utenti
        else:
            ultimate_goal_distance = users_logged[ultimate_goal[i]][2]  # lista delle distanze dell'ultima posizione che visitera' il robot
            for usr in users_logged:
                usr_index = users_logged[usr][0]  # indice dell'utente
                res[usr].append(ultimate_goal_distance[usr_index])
    return res


def delivery_scheduler1(dist_dict):  # scheduler non ottimizzato: assegna a turno un obiettivo ad ogni robot finche' la coda delle richieste non viene svuotata
    global n_robot
    global delivery_requests
    global users_logged
    
    # creazione preventiva della lista per evitare errori
    robot_q = []
    for i in range(n_robot):
        robot_q.append([])
    
    j = 0
    for i in range(len(delivery_requests)):
        robot_q[j].append(delivery_requests[i])
        j = (j+1) % n_robot
    
    total_distance = 0
    for i in range(n_robot):
        
        # se c'e' una sola richiesta il programma non deve crashare
        try:
            total_distance += dist_dict[robot_q[i][0][0]][i]  # distanza tra il robot e la prima richiesta
        except:
            continue
        
        for j in range(len(robot_q[i]) - 1):
            sender = robot_q[i][j][0]
            sender_index = users_logged[sender][0]
            next_sender = robot_q[i][j+1][0]
            next_sender_index = users_logged[next_sender][0]
            receiver = robot_q[i][j][1]
            receiver_distance_list = users_logged[receiver][2]
            total_distance += receiver_distance_list[sender_index] + receiver_distance_list[next_sender_index]
        
        sender = robot_q[i][len(robot_q[i]) - 1][0]
        sender_index = users_logged[sender][0]
        receiver = robot_q[i][len(robot_q[i]) - 1][1]
        receiver_distance_list = users_logged[receiver][2]
        total_distance += receiver_distance_list[sender_index]
    
    return robot_q, total_distance


def create_data_model(dist):
    global n_robot
    global users_logged
    global delivery_requests
    
    data = {}
    data['distance_matrix'] = []
    data['pickups_deliveries'] = []
    data['starts'] = []
    data['ends'] = []
    data['num_vehicles'] = n_robot
    
    for i in range(n_robot):
        data['starts'].append(i + 1)
        data['ends']. append(0)
    
    users_logged_copy = deepcopy(users_logged)
    dist_dict = deepcopy(dist)
    
    # or-tools non funziona con valori float, quindi vengono trasformati in interi cercando di mantenere le proporzioni
    for key in dist_dict:
        for i in range(n_robot):
            dist_dict[key][i] = int(dist_dict[key][i] * 1000)
    # or-tools non funziona con valori float, quindi vengono trasformati in interi cercando di mantenere le proporzioni
    for key in users_logged_copy:
        users_logged_copy[key][2] = [int(i*1000) for i in users_logged_copy[key][2]]
    
    distance_matrix = {}  # creazione dizionario distance_matrix
    matrix_length = (len(delivery_requests) * 2) + 1 + n_robot  # calcolo grandezza totale della matrice delle distanze
    
    distance_matrix["end"] = [0] * matrix_length  # la base di arrivo (arbitraria) viene considerata come distante 0 da tutto il resto
    data['distance_matrix'].append(distance_matrix["end"])  # la base di arrivo viene aggiunta a data
    
    # inserimento in data delle posizioni iniziali dei robot
    for i in range(n_robot):
        key = "start" + str(i)  # composizione della chiave per la posizione iniziale dei robot
        
        distance_matrix[key] = [0]  # la distanza dalla posizione finale e' sempre 0 perche' arbitraria
        for j in range(n_robot):
            if j == i:
                distance_matrix[key].append(0)  # la distanza da stessi e' sempre nulla
            else:
                distance_matrix[key].append(maxsize)  # la distanza dalle altre posizioni dei robot puo' essere qualunque. Con maxsize mi assicura che non venga mai presa in considerazione

        for de_req in delivery_requests:
            distance_matrix[key].append(dist_dict[de_req[0]][i])  # aggiunta della distanza dal robot al sender della richiesta
            distance_matrix[key].append(dist_dict[de_req[1]][i])  # aggiunta della distanza dal robot al receiver della richiesta
        
        data['distance_matrix'].append(distance_matrix[key])  # a conclusione vengono aggiunte le distanze a data
    
    # creazione dei dizionari da inserire in data 
    for usr in users_logged_copy:
        usr_distance_list = users_logged_copy[usr][2]  # estrazione lista delle distanze rispetto al singolo utente
        distance_matrix[usr] = [0]  # la distanza dalla fine e' sempre nulla
        
        for i in range(n_robot):
            distance_matrix[usr].append(dist_dict[usr][i])  # aggiunta della distanza tra i robot e l'utente
        
        # aggiunta di tutte le distanze tra tutti i robot per ogni richiesta
        for de_req in delivery_requests:
            other_usr_index = users_logged_copy[de_req[0]][0]  # estrazione indice del sender della richiesta
            distance_matrix[usr].append(usr_distance_list[other_usr_index])  # aggiunta della distanza tra il sender e l'utente preso in considerazione all'inizio
            other_usr_index = users_logged_copy[de_req[1]][0]  # estrazione indice del receiver della richiesta
            distance_matrix[usr].append(usr_distance_list[other_usr_index])  # aggiunta della distanza tra il receiver e l'utente preso in considerazione all'inizio
    
    # aggiunta a data delle distanze tramite i dizionari creati in precedenza
    for de_req in delivery_requests:
        data['distance_matrix'].append(distance_matrix[de_req[0]])
        data['distance_matrix'].append(distance_matrix[de_req[1]])
    
    # creazione delle richieste di pickup and delivery
    for i in range(len(delivery_requests)):
        data['pickups_deliveries'].append([i*2+3, i*2+4])

    return data


def solution_interpreter(data, manager, routing, solution):
    total_distance = 0
    route = []
    
    # per ogni veicolo viene interpretata la soluzione
    for vehicle_id in range(data['num_vehicles']):
        route.append([])  # creazione di una entry per ogni veicolo
        index = routing.Start(vehicle_id)  # acquisizione punto di partenza della route
        index = solution.Value(routing.NextVar(index))  # acquisizione tappa successiva
        route_distance = 0
        while not routing.IsEnd(index):
            route[vehicle_id].append(manager.IndexToNode(index))  # salvataggio del nodo da visitare
            previous_index = index  # salvataggio del nodo attuale che diventa il precedente
            index = solution.Value(routing.NextVar(index))  # acquisizione tappa successiva
            route_distance += routing.GetArcCostForVehicle(previous_index, index, vehicle_id)  # aggiornamento distanza percorsa
        total_distance += route_distance  # aggiornamento distanza percorsa complessivamente da tutti i robot
    
    return route, float(total_distance) / 1000


def delivery_scheduler2(dist_dict):
    global delivery_requests
    global n_robot

    data = create_data_model(dist_dict)  # creazione del modello da risolvere
    # creazione del routing index manager.
    manager = pywrapcp.RoutingIndexManager(len(data['distance_matrix']), data['num_vehicles'], data['starts'], data['ends'])

    # creazione del routing model.
    routing = pywrapcp.RoutingModel(manager)

    # definizione costo per ogni arco
    def distance_callback(from_index, to_index):
        # conversione da routing variable index a distance matrix NodeIndex
        from_node = manager.IndexToNode(from_index)
        to_node = manager.IndexToNode(to_index)
        return data['distance_matrix'][from_node][to_node]

    transit_callback_index = routing.RegisterTransitCallback(distance_callback)  # dichiarazione di distance_callback come metodo per acquisire distanze
    routing.SetArcCostEvaluatorOfAllVehicles(transit_callback_index)
    # dichiarazione vincoli di distanza
    dimension_name = 'Distance'
    routing.AddDimension(
        transit_callback_index,
        0,  # no slack
        maxsize,  # distanza massima percorribile dai veicoli
        True,  # inizia da 0
        dimension_name)
    distance_dimension = routing.GetDimensionOrDie(dimension_name)
    distance_dimension.SetGlobalSpanCostCoefficient(100)

    # definizione richieste di pickup and delivery
    for request in data['pickups_deliveries']:
        pickup_index = manager.NodeToIndex(request[0])  # dichiarazione del nodo di pickup
        delivery_index = manager.NodeToIndex(request[1])  # dichiarazione del nodo di delivery
        routing.AddPickupAndDelivery(pickup_index, delivery_index)  # coppia aggiunta per la soluzione
        routing.solver().Add(routing.VehicleVar(pickup_index) == routing.VehicleVar(delivery_index))  # il veicolo che raccoglie un pacco e' lo stesso che lo consegna
        routing.solver().Add(distance_dimension.CumulVar(pickup_index) <= distance_dimension.CumulVar(delivery_index))
        
        # pickup -> delivery vincolo di sequenza: ad un pickup consegue immediatamente una delivery
        routing.solver().Add(routing.solver().IsEqualCstCt(routing.NextVar(pickup_index), delivery_index, routing.solver().Min(routing.ActiveVar(pickup_index), routing.ActiveVar(delivery_index))))
    # impostazione prima soluzione euristica
    search_parameters = pywrapcp.DefaultRoutingSearchParameters()
    search_parameters.first_solution_strategy = (routing_enums_pb2.FirstSolutionStrategy.PARALLEL_CHEAPEST_INSERTION)

    # soluzione del problema
    solution = routing.SolveWithParameters(search_parameters)
    # acquisizione dati dalla soluzione
    if solution:
        distance = 0
        route, distance = solution_interpreter(data, manager, routing, solution)
    else:
        return [], maxsize
    
    # comunicazione dell'interpretazione della soluzione ai robot
    robot_q = []
    for i in range(n_robot):
        robot_q.append([])
        for j in range(len(route[i])/2):
            sender = route[i][j*2]
            sender = (sender - 3) / 2
            robot_q[i].append(delivery_requests[sender])
    
    # l'interprete non aggiunge la distanza iniziale tra i robot e il primo target, cosi viene aggiunta a posteriori
    for i in range(n_robot):
        try:
            distance += float(dist_dict[robot_q[i][0][0]][i]) / 1000
        except:
            continue
            
    return robot_q, round(distance, 3)


class thread_sheduler(threading.Thread):  # "override" della classe thread per permettere valori di ritorno al momento del join
    def __init__(self, group=None, target=None, name=None, args=(), kwargs={}, Verbose=None):
        threading.Thread.__init__(self, group, target, name, args, kwargs, Verbose)
        self._return = None
    def run(self):
        if self._Thread__target is not None:
            self._return = self._Thread__target(*self._Thread__args, **self._Thread__kwargs)
    def join(self):
        threading.Thread.join(self)
        return self._return


def scheduler_chooser():  # da modificare nel caso di n scheduler
    global delivery_requests
    global robot_queue
    global n_robot
    
    length = 0  # la lunghezza iniziale

    while not rospy.is_shutdown():
        if len(delivery_requests) > length:
            length = len(delivery_requests)  # aggiornamento lunghezza cosi' il codice viene eseguito una sola volta

            dist_dict = get_distance()
            
            t1 = thread_sheduler(target=delivery_scheduler1, args=[dist_dict])
            t2 = thread_sheduler(target=delivery_scheduler2, args=[dist_dict])
            t1.setDaemon(True)
            t2.setDaemon(True)
            t1.start()
            t2.start()
            queue1, distance1 = t1.join()            
            queue2, distance2 = t2.join()
            
            print("\n")
            print("Distanza da percorrere con lo scheuler_1 " + str(distance1))
            print("Distanza da percorrere con lo scheuler_2 " + str(distance2))
            
            if test:
                result_file = open(package_path + '/scripts/result.txt', 'r')  # apertura file il tettura per inserire la distanza dal robot
                file = result_file.readline()
                index_to_insert = file.split(" ")[len(file.split(" "))-1]
                index_to_insert = int(index_to_insert) + 2
                for line in result_file:
                    file += line
                result_file.close()
                rows = file.split("\n")
                rows = [x + "\n" for x in rows]
                sorted_dict = sorted(users_logged.items(), key=lambda x: x[1], reverse=False)
                row_to_insert = ""
                for i in range(n_robot):
                    row_to_insert += "Distanza dal robot_" + str(i) + ": "
                    for j in range(len(sorted_dict)):
                        row_to_insert += str(dist_dict[sorted_dict[j][0]][i]) + ", "
                    row_to_insert = row_to_insert[:-2] + "\n"
                rows.insert(index_to_insert, row_to_insert)
                result_file = open(package_path + '/scripts/result.txt', 'w')
                result_file.writelines(rows)
                
                
                
                #result_file = open(package_path + '/scripts/result.txt', 'a')  # apertura file per scrivere i risultati
                
                result_file.write("Scheduler sequenziale:\n")
                for i in range(n_robot):
                    result_file.write("Richieste robot_" + str(i) + ":\n")
                    j = 1
                    for request in queue1[i]:
                        row = str(j) + ") " + request[0] + "->" +request[1] + "\n"
                        result_file.write(row)
                        j += 1
                result_file.write("Distanza totale percorsa dai due robot: " + str(distance1) + "\n\n")
                    
                result_file.write("Scheduler ottimizzato:\n")
                for i in range(n_robot):
                    result_file.write("Richieste robot_" + str(i) + ":\n")
                    j = 1
                    for request in queue2[i]:
                        row = str(j) + ") " + request[0] + "->" +request[1] + "\n"
                        result_file.write(row)
                        j += 1
                result_file.write("Distanza totale percorsa dai due robot: " + str(distance2))
            
            if distance1 < distance2:
                robot_queue = queue1
                print("E' stato scelto lo scheduler 1. Distanza da percorrere: " + str(distance1))
                continue
            
            robot_queue = queue2
            print("E' stato scelto lo scheduler 2. Distanza da percorrere: " + str(distance2))
        elif len(delivery_requests) < length:
            length = len(delivery_requests)
    return


def do_test():
    global delivery_requests
    global users_logged
    global ultimate_goal
    global server_map
    
    print("E' stato scelto di effettuare un test")
    result_file = open(package_path + '/scripts/result.txt', 'w')  # creazione file vuoto per i risultati
    
    names = ["topolino", "paperino", "pluto", "pippo", "minnie", "gambadilegno", "orazio", "gastone", "clarabella", "petulia"]
    height = server_map.info.height  # altezza della mappa
    width = server_map.info.width  # larghezza della mappa
    
    length = random.randint(4, len(names)-1)  # estrazione casuale numero di utenti
    max_combination = length * (length - 1)  # semplificazione di n!/(n-2)!
    number_combination = random.randint(5, max_combination - 1)
    
    n = length
    i = 0
    
    print(length)
        
    for i in range(length):
        j = random.randint(0, n)
        usr = names.pop(j)
        users_logged[usr] = [i]
        flag = True
        while flag:
            x = random.randint(0, width-1)
            y = random.randint(0, height-1)
            xi = x
            yi = y
            if server_map.info.origin.orientation.w == 1:
                xi = y
                yi = x
            index = server_aux.index(xi, yi, width)
            if server_map.data[index] is not 0:
                continue
            
            x = float(x) * server_map.info.resolution + server_map.info.origin.position.x  # conversione indici della matrice a coordinate
            y = float(y) * server_map.info.resolution + server_map.info.origin.position.y  # conversione indici della matrice a coordinate
            flag = False

        users_logged[usr].append([x, y])
        n -= 1
    
    sorted_dict = sorted(users_logged.items(), key=lambda x: x[1], reverse=False)
    destination_coords = []
    for i in range(len(sorted_dict)):
        destination_coords.append(sorted_dict[i][1][1])
    
    i = 0
    for coords in destination_coords:
        print("Calcolo distanze di " + sorted_dict[i][0])
        dist = server_aux.dijkstra(server_map, deepcopy(coords), deepcopy(destination_coords))
        users_logged[sorted_dict[i][0]].append(dist)
        i += 1
    
    names = list(users_logged.keys())
    i = 0
    app = []
    while i < number_combination:
        flag = True
        while flag:
            n1 = random.randint(0, length - 1)
            n2 = random.randint(0, length - 1)
            if n1 == n2:
                continue
            request = [names[n1], names[n2]]
            if request in app:
                continue
            flag = False
        app.append(request)
        i += 1
    
    # scrittura risultati su file
    result_file.write("Numero di utenti estratto: " + str(length) + "\n")
    result_file.write("Lista degli utenti estratti:\n")
    for i in range(len(sorted_dict)):  # scrittura di utenti e relative distanze
        usr = sorted_dict[i][0]
        row = str(i+1) + ") " + usr + "\tlista distanze: "
        for x in users_logged[usr][2]:
            row += str(x) + ", "
        result_file.write(row[:-2] + "\n")
    result_file.write("\nNumero di richieste estratto: " + str(number_combination) + "\n")
    result_file.write("Lista delle richieste estratte:\n")
    row = ""
    i = 1
    for req in app:  # scruttra delle richieste
        row += str(i) + ") " + req[0] + "->" + req[1] + "\n"
        i += 1
    result_file.write(row)
    result_file.close()

    delivery_requests = app


if __name__ == "__main__":
    init()
    # nel caso in cui il file degli utenti non esiste viene creato
    users_file = open(package_path + '/scripts/users.txt', 'a')
    users_file.close()
    
    # avvio modulo per scegliere lo scheduler migliore
    t = threading.Thread(target=scheduler_chooser)
    t.setDaemon(True)
    t.start()
    
    # avvio dispiatcher per ogni robot
    for i in range(n_robot):
        t = threading.Thread(target=robot_dispatch, args=[i])
        t.setDaemon(True)
        t.start()

    # in fase di testing o dimostrazione tutte le connessioni con i client vengono saltate
    if test:
        do_test()
        rospy.spin()
    
    # impostazione dei parametri del server
    server_port = 12000
    server_socket = socket(AF_INET, SOCK_STREAM)  # creazione socket TCP
    server_socket.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)  # socket riutilizzabile
    server_socket.bind(('', server_port))  # bind socket alla porta
    server_socket.listen(1)  # socket messo in ascolto
    server_socket.settimeout(None)  # timeout impostato in blocking mode

    # avvio dei servizi per server per ogni utente
    while not rospy.is_shutdown():
        print("Server disponibile a ricevere connessioni")
        connection_socket, addr = server_socket.accept()  # server in attesa delle connessioni in arrivo
        connection_socket.settimeout(None)  # timeout impostato in blocking mode
        print("Connessione stabilita\n")
        t = threading.Thread(target=server_service, args=[connection_socket])
        t.setDaemon(True)
        t.start()

"""
TODO
"""