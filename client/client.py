from socket import socket, AF_INET, SOCK_STREAM
from psutil import Process

import multiprocessing
import os
import sys
import signal


global username
username = ""


def handler(signum, frame):
    exit(0)


def connection():
    server_name = 'localhost'
    server_port = 12000
    client_socket = socket(AF_INET, SOCK_STREAM)  # creazione socket TCP
    client_socket.connect((server_name, server_port))  # connessione al server remoto (serverName, serverPort)
    return client_socket


def registration(client_socket):
    # inserimento e convalida username
    while True:
        usr_input = raw_input("Inserire nome utente:\n")
        if len(usr_input.split()) > 1:
            print("\nIl nome utente non puo' contenere spazi")
            continue
        client_socket.send(usr_input)  # invio dati al server
        sentence = str(client_socket.recv(1024))  # ricezione risposta dal server
        if sentence == "1":
            break
        else:
            print("\nNome utente gia' esistente")

    buffer = raw_input("Inserire password:\n")  # inserimento password
    client_socket.send(buffer)  # invio dati al server

    # inserimento e convalida coordinata X
    while True:
        buffer = raw_input("Inserire coordinata X:\n")  # inserimento coordinata X
        try:
            float(buffer)
        except Exception:
            print("\nInput non valido")
        else:
            client_socket.send(buffer)  # invio dati al server
            break

    # inserimento e convalida coordinata Y
    while True:
        buffer = raw_input("Inserire coordinata Y:\n")  # inserimento coordinata Y
        try:
            float(buffer)
        except Exception:
            print("\nInput non valido")
        else:
            client_socket.send(buffer)  # invio dati al server
            break

    sentence = str(client_socket.recv(1024))  # ricezione risposta dal server
    if sentence == "1":
        print("Registrazione effettuata con successo")
        global username
        username = usr_input
    else:
        print("Registrazione fallita per qualche motivo")


def login(client_socket):
    usr = raw_input("Inserire nome utente:\n")
    client_socket.send(usr)  # invio dati al server
    buffer = raw_input("Inserire password:\n")
    client_socket.send(buffer)  # invio dati al server

    sentence = str(client_socket.recv(1024))  # ricezione risposta dal server
    if sentence == "1":
        print("Login effettuato con successo\n")
        global username
        username = usr
    elif sentence == "2":
        print("Username o password errati")
    elif sentence == "3":
        print("Questo nome utente e' gia' online")
    return


def input_process(client_socket, stdin):
    while True:
        # acquisizione input da stdin, relativo controllo per evitare crash per conversione
        print("\nInserire:\n1) per inviare un pacco\n2) per il logout")
        try:
            request = str(int(stdin.readline()))
        except:
            print("\nInput non valido")
            continue

        # richiesta di invio pacco
        if request == "1":
            client_socket.send("1")
            return

        # richiesta di logout
        elif request == "2":
            client_socket.send("2")  # comunicazione al server
            pp = Process(os.getppid())  # acquisizione id processo genitore
            pp.terminate()  # chiusura processo genitore
            client_socket.close()  # chiusura connessione
            return

        else:
            print("\nInput non valido")


def package_shipping(client_socket):    
    users = []
    client_socket.send("--")
    while True:
        user = str(client_socket.recv(1024))  # ricezione username dal server
        if user == "1 1":  # controllo di segnale di fine invio
            break
        users.append(user)  # aggiunta di username alla lista degli utenti online
        client_socket.send("--")  # invio dati a caso per sincronizzazione con il server

    print("\n")
    while True:
        print("Inserire il numero relativo all'utente a cui si vuole inviare il pacco\n0) Annulla invio pacco")
        
        # stampa di tutti gli user registrati
        i = 0
        for user in users:
            i += 1
            print(str(i) + ") " + user)

        # acquisizione input di invio o annullamento
        try:
            request = int(raw_input())
        except:
            print("\nInput non valido")
            continue
        
        if request > i or request < 0:
            print("\nInput non valido")
            continue
        if request == 0:
            client_socket.send("1 1")  # segnale di annullamento
            return

        client_socket.send(users[request-1])  # invio richiesta al server
        sentence = client_socket.recv(1024)  # ricezione risposta dal server
        
        if sentence == "1":
            print("Il server ha preso in incarico la richiesta")
        elif sentence == "2":
            print("Questa richiesta e' gia' in coda")
        else:
            print("Per qualche motivo la richiesta non e' stata accettata")
        
        break
    return


if __name__ == "__main__":
    signal.signal(signal.SIGINT, handler)
    client_socket = connection()
    while True:
        request = raw_input("Inserire:\n1) per effettuare il login\n2) per effettuare una registrazione\n")
        if request == "1":
            client_socket.send(request)  # invio risposta al server
            login(client_socket)
            if username == "":
                print("\nImpossibile effetuare il login")
                continue
            break
                
        elif request == "2":
            client_socket.send(request)  # invio risposta al server
            registration(client_socket)
            break
        else:
            print("\nInput non valido")

    print("Benvenuto " + username)
    
    newstdin = os.fdopen(os.dup(sys.stdin.fileno()))  # creazione canale di input per il processo figlio
    p = multiprocessing.Process(target=input_process, args=(client_socket, newstdin))  # creazione processo figlio
    p.daemon = True
    p.start()  # avvio processo figlio

    # client in ascolto di eventuali comunicazioni dal server
    while True:
        socket_data = str(client_socket.recv(1024))
        
        try:
            sentence = socket_data[0]
            socket_data = socket_data[1:]
        except:
            exit(0)

        # richista del client di mettere in coda una consegna
        if sentence == "1":
            p.terminate()  # chiusura forzara del processo di input
            p.join()  # aspetto per sicurezza che il processo sia chiuso
            newstdin.close()  # chiusura del canale di input del processo
            
            package_shipping(client_socket)
                
            # richiesta di spedizione completata. il client ritorna in ascolto di segnali dal server o di nuovi comandi dall'utente
            newstdin = os.fdopen(os.dup(sys.stdin.fileno()))  # creazione canale di input per il processo figlio
            p = multiprocessing.Process(target=input_process, args=(client_socket, newstdin))  # creazione processo figlio
            p.daemon = True
            p.start()  # avvio processo figlio

        # comunicazione del server che il client puo' caricare un pacchetto
        elif sentence == "2":
            client_socket.send("3")  # invio segnale per bloccare la disponibilita' del client
            p.terminate()  # chiusura forzara del processo di input
            newstdin.close()  # chiusura del canale di input del processo
                
            # caricamento pacco sul robot
            while True:
                request = raw_input("\nIl robot ha raggiunto la tua posizione.\n\nInserire:\n1) per consegnare il pacco\n2) per annullare la spedizione\n")
                try:
                    if request == "1":
                        client_socket.send("1")  # segnale di caricamento
                        client_socket.send("")  # serve a verificare che la connessione sia ancora attiva
                        print("Pacco caricato")
                        break
                    elif request == "2":
                        client_socket.send("2")  # segnale di annullamento
                        client_socket.send("")  # serve a verificare che la connessione sia ancora attiva
                        print("Spedizione annullata")
                        break
                    else:
                        print("\nInput non valido")
                        continue
                except:
                    print("COnnessione chiusa da parte del server")
                    exit(1)
            
            # richiesta di spedizione completata. il client ritorna in ascolto di segnali dal server o di nuovi comandi dall'utente
            newstdin = os.fdopen(os.dup(sys.stdin.fileno()))  # creazione canale di input per il processo figlio
            p = multiprocessing.Process(target=input_process, args=(client_socket, newstdin))  # creazione processo figlio
            p.daemon = True
            p.start()  # avvio processo figlio
        
        # comunicazione del server che l'invio e' fallito
        elif sentence == "3":
            print("Il robot non e' riuscito a trovarti. Spedizione verso " + socket_data + " annullata\n")
            print("\nInserire:\n1) per inviare un pacco\n2) per il logout")  # piccolo walk_around per non richiamare la funzione di input
            
        elif sentence == "4":
            client_socket.send("3")  # invio segnale per bloccare la disponibilita' del client
            p.terminate()  # chiusura forzata del processo figlio
            newstdin.close()  # chiusura del canale di input del processo
            
            while True:
                request = raw_input("\nUn robot vuole consegnarti un pacco. Inserire:\n1) per ritirare il pacco\n2) per rispedire al mittente\n")
                if request == "1":
                    client_socket.send("1")  # segnale di caricamento
                    try:
                        client_socket.send("")  # test per verifgicare se la connessione e' ancora attiva
                    except:
                        print("Connessione chiusa da parte del server")
                        exit(0)
                    print("Pacco accettato")
                    break
                elif request == "2":
                    client_socket.send("2")  # segnale di annullamento
                    print("Pacco rifiutato e rispedito al mittente")
                    break
                else:
                    print("\nInput non valido")
                    continue
            
            # il client ritorna in ascolto di segnali dal server o di nuovi comandi dall'utente
            newstdin = os.fdopen(os.dup(sys.stdin.fileno()))  # creazione canale di input per il processo figlio
            p = multiprocessing.Process(target=input_process, args=(client_socket, newstdin))  # creazione processo figlio
            p.daemon = True
            p.start()  # avvio processo figlio
        
        elif sentence == "3":
            client_socket.send("1")
            target = client_socket.recv()
            print("Il robot non e' riuscito a raggiungerti. Spedizione verso " + target + "annullata!")
            print("Inserire:\n1) per inviare un pacco\n2) per il logout")  # piccolo walk_around per non richiamare la funzione di input