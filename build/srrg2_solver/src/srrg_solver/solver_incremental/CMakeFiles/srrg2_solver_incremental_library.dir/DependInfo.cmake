# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/federico/labiagi_project/src/srrg2_solver/srrg2_solver/src/srrg_solver/solver_incremental/factor_graph_incremental_sorter.cpp" "/home/federico/labiagi_project/build/srrg2_solver/src/srrg_solver/solver_incremental/CMakeFiles/srrg2_solver_incremental_library.dir/factor_graph_incremental_sorter.cpp.o"
  "/home/federico/labiagi_project/src/srrg2_solver/srrg2_solver/src/srrg_solver/solver_incremental/instances.cpp" "/home/federico/labiagi_project/build/srrg2_solver/src/srrg_solver/solver_incremental/CMakeFiles/srrg2_solver_incremental_library.dir/instances.cpp.o"
  "/home/federico/labiagi_project/src/srrg2_solver/srrg2_solver/src/srrg_solver/solver_incremental/solver_incremental.cpp" "/home/federico/labiagi_project/build/srrg2_solver/src/srrg_solver/solver_incremental/CMakeFiles/srrg2_solver_incremental_library.dir/solver_incremental.cpp.o"
  "/home/federico/labiagi_project/src/srrg2_solver/srrg2_solver/src/srrg_solver/solver_incremental/solver_incremental_base.cpp" "/home/federico/labiagi_project/build/srrg2_solver/src/srrg_solver/solver_incremental/CMakeFiles/srrg2_solver_incremental_library.dir/solver_incremental_base.cpp.o"
  "/home/federico/labiagi_project/src/srrg2_solver/srrg2_solver/src/srrg_solver/solver_incremental/solver_incremental_running_environment.cpp" "/home/federico/labiagi_project/build/srrg2_solver/src/srrg_solver/solver_incremental/CMakeFiles/srrg2_solver_incremental_library.dir/solver_incremental_running_environment.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROS_BUILD_SHARED_LIBS=1"
  "SRRG2_SOLVER_DATA_FOLDER=\"/home/federico/labiagi_project/src/srrg2_solver/srrg2_solver/examples/data\""
  "SRRG2_SOLVER_EXAMPLE_FOLDER=\"/home/federico/labiagi_project/src/srrg2_solver/srrg2_solver/examples\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/federico/labiagi_project/src/srrg2_solver/srrg2_solver/src"
  "/usr/include/suitesparse"
  "/usr/include/eigen3"
  "/home/federico/labiagi_project/src/srrg_cmake_modules/cmake_modules"
  "/home/federico/labiagi_project/src/srrg2_core/srrg2_core/src"
  "/usr/include/opencv"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/federico/labiagi_project/build/srrg2_solver/src/srrg_solver/utils/CMakeFiles/srrg2_solver_factor_graph_utils_library.dir/DependInfo.cmake"
  "/home/federico/labiagi_project/build/srrg2_solver/src/srrg_solver/variables_and_factors/types_projective/CMakeFiles/srrg2_solver_projective_library.dir/DependInfo.cmake"
  "/home/federico/labiagi_project/build/srrg2_solver/src/srrg_solver/variables_and_factors/types_calib/CMakeFiles/srrg2_solver_calib_library.dir/DependInfo.cmake"
  "/home/federico/labiagi_project/build/srrg2_solver/src/srrg_solver/variables_and_factors/types_2d/CMakeFiles/srrg2_solver_types2d_library.dir/DependInfo.cmake"
  "/home/federico/labiagi_project/build/srrg2_solver/src/srrg_solver/variables_and_factors/types_3d/CMakeFiles/srrg2_solver_types3d_library.dir/DependInfo.cmake"
  "/home/federico/labiagi_project/build/srrg2_solver/src/srrg_solver/solver_core/CMakeFiles/srrg2_solver_core_library.dir/DependInfo.cmake"
  "/home/federico/labiagi_project/build/srrg2_solver/src/srrg_solver/solver_core/internals/linear_solvers/CMakeFiles/srrg2_solver_linear_solvers_library.dir/DependInfo.cmake"
  "/home/federico/labiagi_project/build/srrg2_solver/src/srrg_solver/solver_core/internals/sparse_block_matrix/CMakeFiles/srrg2_solver_sparse_block_matrix_library.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
