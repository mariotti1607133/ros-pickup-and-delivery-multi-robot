# CMake generated Testfile for 
# Source directory: /home/federico/labiagi_project/src/srrg2_solver/srrg2_solver/src/srrg_solver
# Build directory: /home/federico/labiagi_project/build/srrg2_solver/src/srrg_solver
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("solver_core")
subdirs("variables_and_factors")
subdirs("utils")
subdirs("solver_incremental")
