# CMake generated Testfile for 
# Source directory: /home/federico/labiagi_project/src/srrg2_core/srrg2_core_ros/src
# Build directory: /home/federico/labiagi_project/build/srrg2_core_ros/src
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("app")
subdirs("node")
subdirs("srrg_converters")
subdirs("srrg_messages_ros")
subdirs("srrg_viewer_ros")
subdirs("tests")
