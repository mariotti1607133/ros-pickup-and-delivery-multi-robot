#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/federico/labiagi_project/devel/.private/srrg_joystick_teleop:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/home/federico/labiagi_project/devel/.private/srrg_joystick_teleop/lib:$LD_LIBRARY_PATH"
export PKG_CONFIG_PATH="/home/federico/labiagi_project/devel/.private/srrg_joystick_teleop/lib/pkgconfig:$PKG_CONFIG_PATH"
export PWD='/home/federico/labiagi_project/build/srrg_joystick_teleop'
export ROSLISP_PACKAGE_DIRECTORIES="/home/federico/labiagi_project/devel/.private/srrg_joystick_teleop/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/federico/labiagi_project/src/srrg_joystick_teleop:/home/federico/Scaricati/labiagi_project/src/srrg_joystick_teleop:$ROS_PACKAGE_PATH"