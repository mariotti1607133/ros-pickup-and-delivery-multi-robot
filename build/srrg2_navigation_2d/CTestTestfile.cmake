# CMake generated Testfile for 
# Source directory: /home/federico/labiagi_project/src/srrg2_navigation_2d/srrg2_navigation_2d
# Build directory: /home/federico/labiagi_project/build/srrg2_navigation_2d
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("src")
