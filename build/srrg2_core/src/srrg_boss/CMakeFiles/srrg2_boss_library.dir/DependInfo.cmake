# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/federico/labiagi_project/src/srrg2_core/srrg2_core/src/srrg_boss/blob.cpp" "/home/federico/labiagi_project/build/srrg2_core/src/srrg_boss/CMakeFiles/srrg2_boss_library.dir/blob.cpp.o"
  "/home/federico/labiagi_project/src/srrg2_core/srrg2_core/src/srrg_boss/deserializer.cpp" "/home/federico/labiagi_project/build/srrg2_core/src/srrg_boss/CMakeFiles/srrg2_boss_library.dir/deserializer.cpp.o"
  "/home/federico/labiagi_project/src/srrg2_core/srrg2_core/src/srrg_boss/id_context.cpp" "/home/federico/labiagi_project/build/srrg2_core/src/srrg_boss/CMakeFiles/srrg2_boss_library.dir/id_context.cpp.o"
  "/home/federico/labiagi_project/src/srrg2_core/srrg2_core/src/srrg_boss/id_placeholder.cpp" "/home/federico/labiagi_project/build/srrg2_core/src/srrg_boss/CMakeFiles/srrg2_boss_library.dir/id_placeholder.cpp.o"
  "/home/federico/labiagi_project/src/srrg2_core/srrg2_core/src/srrg_boss/identifiable.cpp" "/home/federico/labiagi_project/build/srrg2_core/src/srrg_boss/CMakeFiles/srrg2_boss_library.dir/identifiable.cpp.o"
  "/home/federico/labiagi_project/src/srrg2_core/srrg2_core/src/srrg_boss/json_object_writer.cpp" "/home/federico/labiagi_project/build/srrg2_core/src/srrg_boss/CMakeFiles/srrg2_boss_library.dir/json_object_writer.cpp.o"
  "/home/federico/labiagi_project/src/srrg2_core/srrg2_core/src/srrg_boss/json_recursive_object_parser.cpp" "/home/federico/labiagi_project/build/srrg2_core/src/srrg_boss/CMakeFiles/srrg2_boss_library.dir/json_recursive_object_parser.cpp.o"
  "/home/federico/labiagi_project/src/srrg2_core/srrg2_core/src/srrg_boss/object_data.cpp" "/home/federico/labiagi_project/build/srrg2_core/src/srrg_boss/CMakeFiles/srrg2_boss_library.dir/object_data.cpp.o"
  "/home/federico/labiagi_project/src/srrg2_core/srrg2_core/src/srrg_boss/object_parser.cpp" "/home/federico/labiagi_project/build/srrg2_core/src/srrg_boss/CMakeFiles/srrg2_boss_library.dir/object_parser.cpp.o"
  "/home/federico/labiagi_project/src/srrg2_core/srrg2_core/src/srrg_boss/object_writer.cpp" "/home/federico/labiagi_project/build/srrg2_core/src/srrg_boss/CMakeFiles/srrg2_boss_library.dir/object_writer.cpp.o"
  "/home/federico/labiagi_project/src/srrg2_core/srrg2_core/src/srrg_boss/serializable.cpp" "/home/federico/labiagi_project/build/srrg2_core/src/srrg_boss/CMakeFiles/srrg2_boss_library.dir/serializable.cpp.o"
  "/home/federico/labiagi_project/src/srrg2_core/srrg2_core/src/srrg_boss/serialization_context.cpp" "/home/federico/labiagi_project/build/srrg2_core/src/srrg_boss/CMakeFiles/srrg2_boss_library.dir/serialization_context.cpp.o"
  "/home/federico/labiagi_project/src/srrg2_core/srrg2_core/src/srrg_boss/serializer.cpp" "/home/federico/labiagi_project/build/srrg2_core/src/srrg_boss/CMakeFiles/srrg2_boss_library.dir/serializer.cpp.o"
  "/home/federico/labiagi_project/src/srrg2_core/srrg2_core/src/srrg_boss/stream.cpp" "/home/federico/labiagi_project/build/srrg2_core/src/srrg_boss/CMakeFiles/srrg2_boss_library.dir/stream.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROS_BUILD_SHARED_LIBS=1"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/federico/labiagi_project/src/srrg_cmake_modules/cmake_modules"
  "/usr/include/opencv"
  "/usr/include/eigen3"
  "/home/federico/labiagi_project/src/srrg2_core/srrg2_core/src"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
