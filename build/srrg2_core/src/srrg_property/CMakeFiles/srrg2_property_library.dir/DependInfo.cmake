# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/federico/labiagi_project/src/srrg2_core/srrg2_core/src/srrg_property/property.cpp" "/home/federico/labiagi_project/build/srrg2_core/src/srrg_property/CMakeFiles/srrg2_property_library.dir/property.cpp.o"
  "/home/federico/labiagi_project/src/srrg2_core/srrg2_core/src/srrg_property/property_container.cpp" "/home/federico/labiagi_project/build/srrg2_core/src/srrg_property/CMakeFiles/srrg2_property_library.dir/property_container.cpp.o"
  "/home/federico/labiagi_project/src/srrg2_core/srrg2_core/src/srrg_property/property_container_manager.cpp" "/home/federico/labiagi_project/build/srrg2_core/src/srrg_property/CMakeFiles/srrg2_property_library.dir/property_container_manager.cpp.o"
  "/home/federico/labiagi_project/src/srrg2_core/srrg2_core/src/srrg_property/property_eigen.cpp" "/home/federico/labiagi_project/build/srrg2_core/src/srrg_property/CMakeFiles/srrg2_property_library.dir/property_eigen.cpp.o"
  "/home/federico/labiagi_project/src/srrg2_core/srrg2_core/src/srrg_property/property_identifiable.cpp" "/home/federico/labiagi_project/build/srrg2_core/src/srrg_property/CMakeFiles/srrg2_property_library.dir/property_identifiable.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROS_BUILD_SHARED_LIBS=1"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/federico/labiagi_project/src/srrg_cmake_modules/cmake_modules"
  "/usr/include/opencv"
  "/usr/include/eigen3"
  "/home/federico/labiagi_project/src/srrg2_core/srrg2_core/src"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/federico/labiagi_project/build/srrg2_core/src/srrg_boss/CMakeFiles/srrg2_boss_library.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
