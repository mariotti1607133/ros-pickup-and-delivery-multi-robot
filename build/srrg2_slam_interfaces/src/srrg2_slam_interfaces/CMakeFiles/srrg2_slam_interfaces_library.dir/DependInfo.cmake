# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/federico/labiagi_project/src/srrg2_slam_interfaces/srrg2_slam_interfaces/src/srrg2_slam_interfaces/instances.cpp" "/home/federico/labiagi_project/build/srrg2_slam_interfaces/src/srrg2_slam_interfaces/CMakeFiles/srrg2_slam_interfaces_library.dir/instances.cpp.o"
  "/home/federico/labiagi_project/src/srrg2_slam_interfaces/srrg2_slam_interfaces/src/srrg2_slam_interfaces/mapping/local_map.cpp" "/home/federico/labiagi_project/build/srrg2_slam_interfaces/src/srrg2_slam_interfaces/CMakeFiles/srrg2_slam_interfaces_library.dir/mapping/local_map.cpp.o"
  "/home/federico/labiagi_project/src/srrg2_slam_interfaces/srrg2_slam_interfaces/src/srrg2_slam_interfaces/raw_data_preprocessors/raw_data_preprocessor_odom.cpp" "/home/federico/labiagi_project/build/srrg2_slam_interfaces/src/srrg2_slam_interfaces/CMakeFiles/srrg2_slam_interfaces_library.dir/raw_data_preprocessors/raw_data_preprocessor_odom.cpp.o"
  "/home/federico/labiagi_project/src/srrg2_slam_interfaces/srrg2_slam_interfaces/src/srrg2_slam_interfaces/registration/aligners/aligner_slice_odometry_prior.cpp" "/home/federico/labiagi_project/build/srrg2_slam_interfaces/src/srrg2_slam_interfaces/CMakeFiles/srrg2_slam_interfaces_library.dir/registration/aligners/aligner_slice_odometry_prior.cpp.o"
  "/home/federico/labiagi_project/src/srrg2_slam_interfaces/srrg2_slam_interfaces/src/srrg2_slam_interfaces/system/map_listener.cpp" "/home/federico/labiagi_project/build/srrg2_slam_interfaces/src/srrg2_slam_interfaces/CMakeFiles/srrg2_slam_interfaces_library.dir/system/map_listener.cpp.o"
  "/home/federico/labiagi_project/src/srrg2_slam_interfaces/srrg2_slam_interfaces/src/srrg2_slam_interfaces/system/multi_graph_slam_messages.cpp" "/home/federico/labiagi_project/build/srrg2_slam_interfaces/src/srrg2_slam_interfaces/CMakeFiles/srrg2_slam_interfaces_library.dir/system/multi_graph_slam_messages.cpp.o"
  "/home/federico/labiagi_project/src/srrg2_slam_interfaces/srrg2_slam_interfaces/src/srrg2_slam_interfaces/trackers/tracker.cpp" "/home/federico/labiagi_project/build/srrg2_slam_interfaces/src/srrg2_slam_interfaces/CMakeFiles/srrg2_slam_interfaces_library.dir/trackers/tracker.cpp.o"
  "/home/federico/labiagi_project/src/srrg2_slam_interfaces/srrg2_slam_interfaces/src/srrg2_slam_interfaces/trackers/tracker_report.cpp" "/home/federico/labiagi_project/build/srrg2_slam_interfaces/src/srrg2_slam_interfaces/CMakeFiles/srrg2_slam_interfaces_library.dir/trackers/tracker_report.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROS_BUILD_SHARED_LIBS=1"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/federico/labiagi_project/src/srrg2_slam_interfaces/srrg2_slam_interfaces/src"
  "/usr/include/eigen3"
  "/home/federico/labiagi_project/src/srrg_cmake_modules/cmake_modules"
  "/home/federico/labiagi_project/src/srrg2_core/srrg2_core/src"
  "/home/federico/labiagi_project/src/srrg2_solver/srrg2_solver/src"
  "/home/federico/labiagi_project/src/srrg_hbst/."
  "/usr/include/opencv"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
