# CMake generated Testfile for 
# Source directory: /home/federico/labiagi_project/src/srrg2_slam_interfaces/srrg2_slam_interfaces
# Build directory: /home/federico/labiagi_project/build/srrg2_slam_interfaces
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("apps")
subdirs("src")
subdirs("tests")
