#!/usr/bin/env python2

from re import U
import rospy
import threading
import server_aux

from socket import socket, AF_INET, SOCK_STREAM
from geometry_msgs.msg import PoseStamped, PoseWithCovarianceStamped
from tf2_msgs.msg import TFMessage
from srrg2_navigation_2d_msgs.msg import PathFollowerStatus      # DA SCEGLIERE TRA QUESTO E IL PLANNER
from srrg2_core_ros.msg import PlannerStatusMessage              # DA SCEGLIERE TRA QUESTO E IL PATHFOLLOWER
from tf2_ros import TransformListener, Buffer
from math import sqrt
from rospkg import RosPack

# da eliminare forse
from nav_msgs.msg import OccupancyGrid  # da eliminare


package_path = RosPack().get_path('pick_and_delivery')
users_logged = {}
n_robot = 2  # numero di robot presenti nella simulazione

#current_position = [0, 0]  finche0' uso pathfollower invece di pitagora non serve
robot_initial_pose = [[49.495300293, 11.6122970581, 0, 0, 0, 0.999974491603, 0.00714255855827], # robot_0 [p.x, p.y, p.z, o.x, o.y, o.z, o.w]
                      [53.4049263, 11.6979093552, 0, 0, 0, -0.00423708500993, 0.999991023515]]  # robot_1 [p.x, p.y, p.z, o.x, o.y, o.z, o.w]
goal_target = []  # lista dei target
goal_status = []  # stati di goal_status: -1->disponibile; 0->cruising; 1->destinazione; 2->fallito
goal_pub = []  # lista dei publisher dei robot per i goal
available = []  # lista della disponibilita' di tutti i robot
initialpose_pub = []  # lista di tutti i publisher per l'initialpose

new_goal_msg = PoseStamped()  # dichiarazione new_goal_msg come variabile di tipo PoseStamped
tfBuffer = Buffer()  # dichiarazione tfBuffer come variabile di tibo tf2_ros.Buffer
current_position = []

seq = 1
t_timeout = 60  # numero di secondi secondi per dare una risposta al server quando si sta bloccando una risorsa
t_delivery = 60  # numero di secondi a disposizione per arrivare a destinazione da quando viene dato un goal
delivery_requests = []  # lista delle richieste. ogni elemento e' composto solo da mittente e destinatario, #### potrebbe essere interessante inserire una coda di priorita


def init():
    global n_robot
    global initialpose_pub
    global goal_pub
    global goal_status
    global goal_target
    global available
    global current_position
    
    rospy.init_node('pick_and_delivery')
    
    for i in range(n_robot):
        initialpose_pub.append(rospy.Publisher("/robot_"+ str(i) + "/initialpose", PoseWithCovarianceStamped, queue_size=10))  # creazione di un publisher initialpose per ogni robot
        goal_pub.append(rospy.Publisher("/robot_"+ str(i) + "/move_base_simple/goal", PoseStamped, queue_size=10))  # creazione di un publisher move_base_simple per ogni robot
        
        goal_target.append([0, 0])  # assegnazione del target 0 per ogni robot
        goal_status.append(-1)  # assegnazione status -1 per ogni robot
        available.append(True)  # impostazione True per ogni robot
        current_position.append([0, 0, 0, 0])  # creazione di 2 oggetti per inserire poi la localizzazione dei robot
        
        #rospy.Subscriber("robot_"+ str(i) + "/path_follower_status", PathFollowerStatus, navigation_callback, (i))  # callback per controllare la navigazione di ogni robot
        rospy.Subscriber("robot_"+ str(i) + "/planner_status", PlannerStatusMessage, navigation_callback, (i))  # callback per controllare la navigazione di ogni robot
    
    TransformListener(tfBuffer)
    rospy.Subscriber("tf", TFMessage, position_callback)
    
    rospy.sleep(1)  # aspetta che ogni publisher abbia i suoi listener
    rospy.Rate(10) # 10hz
    #localization()  # imposta la initial pose del robot, se e' gia stata impostata in precedenza sarebbe meglio commentare la riga
    prova()


def prova():
    distance_matrix = []
    distance_file = open(package_path + '/scripts/distance_matrix.txt', 'r')
    for line in distance_file:
       distance_matrix.append(line)
    distance_file.close()
    #distance_file = open(package_path + '/scripts/distance_matrix.txt', 'w')
    users_file = open(package_path + '/scripts/users.txt', 'r')
    user_coords = {}
    username = users_file.readline()
    line = users_file.readline()
    while line:
        line = users_file.readline()
        username = username.split(" ")
        app = line.split(" ")
        try: 
            user_coords[username[1][:-1]] = [app[1], app[3][:-1]]
        except:
            break
        users_file.readline()
        username = users_file.readline()
        users_file.readline()
    print(user_coords)
    exit()


def localization():
    global seq
    global n_robot
    global initialpose_pub
    
    initial_pose = PoseWithCovarianceStamped()
    initial_pose.header.frame_id = "map"
    initial_pose.header.stamp = rospy.Time.now()
    initial_pose.pose.covariance = [0.25, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.25, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.06853892326654787]

    for i in range(n_robot):
        initial_pose.header.seq = seq
        seq += 1
        
        initial_pose.pose.pose.position.x = robot_initial_pose[i][0]
        initial_pose.pose.pose.position.y = robot_initial_pose[i][1]
        initial_pose.pose.pose.position.z = robot_initial_pose[i][2]
        
        initial_pose.pose.pose.orientation.x = robot_initial_pose[i][3]
        initial_pose.pose.pose.orientation.y = robot_initial_pose[i][4]
        initial_pose.pose.pose.orientation.z = robot_initial_pose[i][5]
        initial_pose.pose.pose.orientation.w = robot_initial_pose[i][6]
        
        initialpose_pub[i].publish(initial_pose)


def registration(connection_socket):
    global users_logged
    
    #convalidazione username
    while True:
        usr_recv = str(connection_socket.recv(1024))  # ricezione username dal socket
        
        # controllo connessione attiva del client
        if usr_recv == "":
            return None

        users_file = open(package_path + '/scripts/users.txt', 'r')
        line = users_file.readline()
        while line:
            usr = line.split()[1]
            if usr == usr_recv:
                connection_socket.send("2")  # invio "2" per comunicare username gia' esistente
                break
            for i in range(3):
                users_file.readline()
            line = users_file.readline()
        
        if not line:
            connection_socket.send("1")  # invio "1" per comunicare username disponibile
            break   

        users_file.close()

    # acquisizione password e coordinate
    pwd_recv = str(connection_socket.recv(1024))  # ricezione password dal socket
    x = str(connection_socket.recv(1024))  # ricezione coordinata X dal socket
    y = str(connection_socket.recv(1024))  # ricezione coordinata Y dal socket

    # controllo connessione attiva del client
    if y == "":
        return None

    # aggiornamento file degli utenti registrati
    users_file = open(package_path + '/scripts/users.txt', 'a')
    users_file.write("username: " + usr_recv + "\n")  # memorizzazione username
    users_file.write("password: " + pwd_recv + "\n")  # memorizzazione password
    users_file.write("x: " + x + " y: " + y + "\n")  # memorizzazione coordinate
    users_file.write("\n")
    users_file.close()
    
    # aggiormaneto file delle distanze
    distance_matrix = []
    distance_file = open(package_path + '/scripts/distance_matrix.txt', 'r')
    for line in distance_file:
       distance_matrix.append(line)
    print(distance_matrix)
    
    connection_socket.send("1")  # invio "1" per comunicare registrazione completata

    users_logged[usr_recv] = [float(x), float(y), connection_socket]
    return usr_recv


def login(connection_socket):
    global users_logged
    
    try:
        users_file = open(package_path + '/scripts/users.txt', 'r')
    except Exception:
        connection_socket.send("2")  # invio "2" per comunicare mancato login
        return None

    usr_recv = str(connection_socket.recv(1024))  # ricezione username dal socket
    pwd_recv = str(connection_socket.recv(1024))  # ricezione password dal socket

    while True:
        try:
            usr_red = users_file.readline().split()[1]  # acquisizione username dal file
            pwd_red = users_file.readline().split()[1]  # acquisizione password dal file
            if usr_recv == usr_red and pwd_recv == pwd_red:
                
                if usr_recv in users_logged:
                    connection_socket.send("3")  # invio "3" per comunicare che esiste gia' un utente online con lo stesso nome
                    return
                
                connection_socket.send("1")  # invio "1" per comunicare confermato login
                
                # lettura coordinate dal file
                row = users_file.readline().split()
                x = float(row[1])
                y = float(row[3])
                users_logged[usr_recv] = [x, y, connection_socket]
                
                return usr_recv
            
            # lettura a vuoto delle coordinate che non interessano
            users_file.readline()
            users_file.readline()
        except Exception:
            connection_socket.send("2")  # invio "2" per comunicare mancato login
            return None


def delivery_acceptance(connection_socket, username):
    global delivery_requests
    """
        scrivere package_delivery: ora deve ricevere tutte le informazioni per la consegna cosi' da schedulare in maniera ottimale i task
        
        scheduling attivita' per raccogliere il pacco e consegnarlo [SE L'UTENTE SI DISCONNETTETE DURANTE QUESTA ATTESA IL SERVER PUO' RIMUOVERE LA SUA RICHIESTA DALLA SCHEDULE]
            idea di scoporare lo scheduler
        consegna pacco al target
        
        fine
    """
    connection_socket.recv(1024)  # ricezione per sincronizzazione con client
    users = users_logged.keys()  # acquisizione di tutti gli utenti online
    users.remove(username)
    
    for user in users:
        connection_socket.send(user)  # invio di 1 nome utente
        connection_socket.recv(1024)  # ricezione per sincronizzare con client
    connection_socket.send("1 1")  # invio segnale di fine
    
    socket_data = connection_socket.recv(1024)  # ricezione utente bersaglio
    if socket_data == "1 1":  # il client ha annullato l'invio del pacco
        return
    
    delivery_requests.append([username, socket_data])  # aggiunta alla lista della richiesta di consegna
    
    return


def set_goal(coords, index_robot):
    global seq

    new_goal_msg = PoseStamped()
    
    # settaggio header
    new_goal_msg.header.frame_id = "map"
    new_goal_msg.header.stamp = rospy.Time.now()
    new_goal_msg.header.seq = seq
    seq += 1    

    # settaggio posizione bersaglio
    new_goal_msg.pose.position.x = coords[0]
    new_goal_msg.pose.position.y = coords[1]
    new_goal_msg.pose.position.z = 0

    # settaggio orientamento bersaglio (w=1 per comodita')
    new_goal_msg.pose.orientation.x = 0
    new_goal_msg.pose.orientation.y = 0
    new_goal_msg.pose.orientation.z = 0
    new_goal_msg.pose.orientation.w = 1

    # aggiornamento variabili di tracking
    goal_target[index_robot][0] = new_goal_msg.pose.position.x
    goal_target[index_robot][1] = new_goal_msg.pose.position.y
    goal_status[index_robot] = 0  # goal_status impostato in cruising
    
    goal_pub[index_robot].publish(new_goal_msg)  # pubblicazione messaggio goal
    NavigationTimer(list(goal_target[index_robot]), int(index_robot))  # avvio timer di consegna


def return_to_sender(username, index_robot):
    global goal_status
    global available
    global users_logged
    
    try:
        set_goal(users_logged[username], index_robot)
    except:
        print("L'utente " + username + " si e' disconnesso e il robot_" + index_robot + " si e' sbarazzato del pacchetto")
        available[index_robot] = True  # riabilitazione del robot ad altre richieste
        return
        
    while goal_status == 0:
        pass
    if goal_status == 1:
        print("Il robot_" + index_robot + " ha riconsegnato forzatamente il pacchetto al mittente")
    elif goal_status == 2:
        print("Il robot_" + index_robot + " si e' sbarazzato del pacco")
    available[index_robot] = True  # riabilitazione del robot ad altre richieste
    return


def position_callback(tfm):
    global current_position
    
    for i in range(n_robot):
        transform_ok = tfBuffer.can_transform("map", "robot_" + str(i) + "/base_link", rospy.Time(0))
        if transform_ok:
            transform_stamped = tfBuffer.lookup_transform("map", "robot_" + str(i) + "/base_link", rospy.Time(0))
            current_position[i][0] = transform_stamped.transform.translation.x
            current_position[i][1] = transform_stamped.transform.translation.y
            current_position[i][2] = transform_stamped.transform.rotation.z
            current_position[i][3] = transform_stamped.transform.rotation.w


"""def navigation_callback(pathfollower_status, index_robot):
    global goal_status
    
    if goal_status[index_robot] == 0:        
        if pathfollower_status.num_steps_to_goal < 3:  # quando mancano meno di 3 step lo considero arrivato a destinazione
            print("Il robot_" + str(index_robot) + " ha raggiunto l'obiettivo")
            # aggiornamento variabile di tracking
            goal_status[index_robot] = 1"""


# "override" di timer per considerare la target position
class NavigationTimer:
    def __init__(self, coords, index):
        global t_delivery
        
        self.this_target = coords
        self.i = index
        rospy.Timer(rospy.Duration(t_delivery), self.timer_CallBack, oneshot=True)  # vero avvio timer di consegna


    def timer_CallBack(self, timer):
        global seq
        global goal_status

        # se il robot non e' in navigazione l'evento non deve fare nulla
        if (goal_status[self.i] is not 0) or (not (goal_target[self.i] == self.this_target)):
            return

        print("TIMEOUT. L'obiettivo non puo' essere raggiunto")
        
        transform_ok = tfBuffer.can_transform("map", "robot_" + str(self.i) + "/base_link", rospy.Time(0))
        while not transform_ok:
            transform_ok = tfBuffer.can_transform("map", "robot_" + str(self.i) + "/base_link", rospy.Time(0))
        
        transform_stamped = tfBuffer.lookup_transform("map", "robot_" + str(self.i) + "/base_link", rospy.Time(0))

        new_goal_msg.header.frame_id = "map"
        new_goal_msg.header.stamp = rospy.Time.now()
        new_goal_msg.header.seq = seq
        seq += 1

        new_goal_msg.pose.position.x = transform_stamped.transform.translation.x
        new_goal_msg.pose.position.y = transform_stamped.transform.translation.y
        new_goal_msg.pose.position.z = 0

        new_goal_msg.pose.orientation.x = 0
        new_goal_msg.pose.orientation.y = 0
        new_goal_msg.pose.orientation.z = 0
        new_goal_msg.pose.orientation.w = 1
        
        # il goal pubblicato e' sulla stessa posizione del robot cosi' si ferma se si trova in movimento
        goal_pub[self.i].publish(new_goal_msg)
        goal_status[self.i] = 2  # eventuale notifica di irraggiungibilita'


def navigation_callback(planner_status, index_robot):
    global goal_status
    
    if goal_status[index_robot] == 0:
        if planner_status.status == "GoalReached" :
            print("Il robot_" + str(index_robot) + " ha raggiunto l'obiettivo")
            goal_status[index_robot] = 1  # aggiornamento status navigazione di tracking


def server_service(connection_socket):   # AAAAAAAAAAAA DA CONTROLLARE INTERAMENTE
    global available
    
    sentence = ""
    username = None

    try:
        # registrazione e login
        while username is None:
            sentence = str(connection_socket.recv(1024))
            if sentence == "1":
                username = login(connection_socket)
                
            elif sentence == "2":
                username = registration(connection_socket)
                
            else:
                return  # probabilmente la connessione e' stata chiusa
        
        print("L'utente " + username + " ha effettuato con successo il login")

        # richieste dal client
        while not rospy.is_shutdown():
            sentence = str(connection_socket.recv(1024))

            # richista di invio pacco da parte del client
            if sentence == "1":
                # controllo disponibilita' robot
                #if available:            AAAAAAAAAAAAA DA CONTROLLARE
                if True:         #             AAAAAAAAAAAAA DA CONTROLLARE
                    #available = False  # disabilitazione del robot ad altre richieste            AAAAAAAAAAAAA DA CONTROLLARE
                    connection_socket.send("1")  # invio conferma accettazione richiesta
                    #send_package(connection_socket, username)    AAAAAAAA DA ELIMINARE
                    delivery_acceptance(connection_socket, username)
                else:
                    connection_socket.send("2")  # invio segnale di robot non disponibile
            
            # richiesta di logout da parte del client
            elif sentence == "2" or sentence == "":
                users_logged.pop(username)  # rimozione dell'utente dalla lista dei connessi
                print("Utente " + username + " disconnesso")
                connection_socket.close()
                return
    except:
        users_logged.pop(username)  # rimozione dell'utente dalla lista dei connessi
        connection_socket.close()
        return
    
    users_logged.pop(username)  # rimozione dell'utente dalla lista dei connessi


def delivery_scheduler():
    
    #while not rospy.is_shutdown():
    return


if __name__ == "__main__":
    init()

    # nel caso in cui il file degli utenti non esiste viene creato
    users_file = open(package_path + '/scripts/users.txt', 'a')
    users_file.close()

    server_port = 12001
    server_socket = socket(AF_INET, SOCK_STREAM)  # creazione socket TCP
    server_socket.bind(('', server_port))  # bind socket alla porta
    server_socket.listen(1)  # socket messo in ascolto
    server_socket.settimeout(None)  # timeout impostato in blocking mode
    
    t1 = threading.Thread(target=delivery_scheduler, args=[])
    t1.start()

    while not rospy.is_shutdown():
        print("Server disponibile a ricevere connessioni")
        connection_socket, addr = server_socket.accept()  # server in attesa delle connessioni in arrivo
        connection_socket.settimeout(None)  # timeout impostato in blocking mode
        print("Connessione stabilita\n")
        t2 = threading.Thread(target=server_service, args=[connection_socket])
        t2.start()


""" MONTE DI FUNZIONI DA RIVEDERE
def position_CallBack(tfm):
    global current_position

    transform_ok = tfBuffer.can_transform("map", "base_link", rospy.Time(0))  # controllo della disponibilita' della trasformata
    if transform_ok:
        transform_stamped = tfBuffer.lookup_transform("map", "base_link", rospy.Time(0))  # calcolo trasformata
        # aggiornamento variabili di tracking
        current_position[0] = transform_stamped.transform.translation.x
        current_position[1] = transform_stamped.transform.translation.y


def sending(username, target, connection_socket_target):
    global goal
    
    setGoal(users_logged[target])
    while goal == 0:
        pass
    if goal == 1:
        try:
            print(target + " raggiunto")
            connection_socket_target.send("3")  # comunicazione al client destinatario che ha ricevuto un pacco
        except:
            print("L'utente " + target + " si e' disconnesso prima dell'arrivo del pacco e non puo' ritirarlo")
            to_sender(username)
            return
    elif goal == 2:
        print("Il robot non e' riuscito a raggiungere l'utente, ritorno al mittente")
        print("Ritorno del pacco al mittente")
        to_sender(username)  # spedizione al mittente
        return
    
    print("Robot in attesa di consegnare il pacco")
    # timeout per il solo utente destinatario
    try:
        connection_socket_target.settimeout(t_timeout)  # per evitare che il client occupi per un tempo indefinito un robot
        sentence = connection_socket_target.recv(1024)  # ricezione risposta dal client
        connection_socket_target.settimeout(None)  # rimozione vincolo temporale per comunicazione con il server
    except:  # disconnessione per inattivita'
        connection_socket_target.close()
        print("Disconnessione forzata di " + target + " per inattivita'")
        print("Ritorno del pacco al mittente")
        to_sender(username)  # spedizione al mittente
        return
    
    if sentence == "1":
        print("Pacco consegnato")
    elif sentence == "2":
        print("Pacco rifiutato")
        to_sender(username)  # spedizione al mittente
    elif sentence == "":
        print("Ritorno del pacco al mittente")
        to_sender(username)  # spedizione al mittente
        

def send_package(connection_socket, username):
    global goal
    global available
    global t_timeout
    
    sentence = ""
    
    setGoal(users_logged[username])
    print("Robot in viaggio verso " + username + " per ricevere il pacco")

    # attesa di un feedback dal sisteam di navigazione
    while goal == 0:
        pass
    if goal == 1:
        connection_socket.send("1")  # invio comunicazione robot arrivato a destinazione
    elif goal == 2:
        connection_socket.send("2")  # invio comunicazione robot disperso
        print("Il robot non e' riuscito a raggiungere l'utente")
        available = True  # riabilitazione del robot ad altre richieste
        return
    
    try:
        print("Robot in attesa di ricevere il pacco")
        connection_socket.settimeout(t_timeout)  # per evitare che il client occupi per un tempo indefinito un robot
        sentence = connection_socket.recv(1024)
    except:  # chiusura connessione per inattivita'
        print("Utente " + username + " disconesso per inattivita'")
        connection_socket.close()
        available = True  # riabilitazione del robot ad altre richieste
        return
    
    if sentence == "1":
        print("Pacco ricevuto")
    elif sentence == "2":
        print("Pacco non caricato")
        available = True  # riabilitazione del robot ad altre richieste
        return
    elif sentence == "":  # chiusura connessione da parte del client
        available = True  # riabilitazione del robot ad altre richieste
        return

    users = users_logged.keys()  # acquisizione di tutti gli utenti online
    users.remove(username)
    connection_socket.send("--")  # invio per sincronia
    for user in users:
        connection_socket.recv(1024)  # ricezione per sincronizzare con client
        connection_socket.send(user)  # invio di 1 nome utente
    connection_socket.recv(1024)  # ricezione per sincronizzare con client
    connection_socket.send("1 1")  # invio segnale di fine

    try:
        request = connection_socket.recv(1024)  # ricezione utente bersaglio
    except:  # chiusura connessione per inattivita'
        print("Utente " + username + " disconesso per inattivita'")
        connection_socket.close()
        available = True  # riabilitazione del robot ad altre richieste
        return
        
    if request == "1 1":  # controllo del segnale di annullamento chiusura connessione
        print("annullamento invio pacco")
        available = True  # riabilitazione del robot ad altre richieste
        return
    elif request == "":  # chiusura connessione da parte del client
        available = True  # riabilitazione del robot ad altre richieste
        return

    print("Spedizione pacco dall'utente " + username + " all'utente " + request)
    connection_socket.settimeout(None)  # rimozione vincolo temporale per comunicazione con il server
    
    t = threading.Thread(target=sending, args=[username, request, users_logged[request][2]])
    t.start()

"""




"""
pathfollower_Status non funziona se il robot e' gia' a destinazione    DA TENERE IN CONSIDERAZIONE IL PLANNER STATUS INVECE
"""


"""
TODO

mi piacerebbe gestire ctrl+c
aggiungere not rospy.is_shutdown(): tra i casi di richiesta del client per vedere se e' stato spento il server: deve chiudere tutte le connessioni
"""