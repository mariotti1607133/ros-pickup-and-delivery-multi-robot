# CMake generated Testfile for 
# Source directory: /home/federico/labiagi_project/src/srrg2_config_visualizer
# Build directory: /home/federico/labiagi_project/build/srrg2_config_visualizer
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("imgui-node-editor/ThirdParty/imgui")
subdirs("imgui-node-editor/NodeEditor")
subdirs("src")
