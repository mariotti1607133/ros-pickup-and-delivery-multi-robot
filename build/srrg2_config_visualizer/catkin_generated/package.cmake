set(_CATKIN_CURRENT_PACKAGE "srrg2_config_visualizer")
set(srrg2_config_visualizer_VERSION "0.4.0")
set(srrg2_config_visualizer_MAINTAINER "Mirco Colosi <colosi@diag.uniroma1.it>")
set(srrg2_config_visualizer_PACKAGE_FORMAT "2")
set(srrg2_config_visualizer_BUILD_DEPENDS "sensor_msgs" "tf" "srrg_cmake_modules" "srrg2_core")
set(srrg2_config_visualizer_BUILD_EXPORT_DEPENDS "sensor_msgs" "tf" "srrg_cmake_modules" "srrg2_core")
set(srrg2_config_visualizer_BUILDTOOL_DEPENDS "catkin")
set(srrg2_config_visualizer_BUILDTOOL_EXPORT_DEPENDS )
set(srrg2_config_visualizer_EXEC_DEPENDS "sensor_msgs" "tf" "srrg_cmake_modules" "srrg2_core")
set(srrg2_config_visualizer_RUN_DEPENDS "sensor_msgs" "tf" "srrg_cmake_modules" "srrg2_core")
set(srrg2_config_visualizer_TEST_DEPENDS )
set(srrg2_config_visualizer_DOC_DEPENDS )
set(srrg2_config_visualizer_URL_WEBSITE "")
set(srrg2_config_visualizer_URL_BUGTRACKER "")
set(srrg2_config_visualizer_URL_REPOSITORY "")
set(srrg2_config_visualizer_DEPRECATED "")