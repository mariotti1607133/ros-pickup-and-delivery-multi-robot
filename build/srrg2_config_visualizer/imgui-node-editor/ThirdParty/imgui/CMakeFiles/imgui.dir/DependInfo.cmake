# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/federico/labiagi_project/src/srrg2_config_visualizer/src/third_party/imgui-node-editor/ThirdParty/imgui/imgui.cpp" "/home/federico/labiagi_project/build/srrg2_config_visualizer/imgui-node-editor/ThirdParty/imgui/CMakeFiles/imgui.dir/imgui.cpp.o"
  "/home/federico/labiagi_project/src/srrg2_config_visualizer/src/third_party/imgui-node-editor/ThirdParty/imgui/imgui_demo.cpp" "/home/federico/labiagi_project/build/srrg2_config_visualizer/imgui-node-editor/ThirdParty/imgui/CMakeFiles/imgui.dir/imgui_demo.cpp.o"
  "/home/federico/labiagi_project/src/srrg2_config_visualizer/src/third_party/imgui-node-editor/ThirdParty/imgui/imgui_draw.cpp" "/home/federico/labiagi_project/build/srrg2_config_visualizer/imgui-node-editor/ThirdParty/imgui/CMakeFiles/imgui.dir/imgui_draw.cpp.o"
  "/home/federico/labiagi_project/src/srrg2_config_visualizer/src/third_party/imgui-node-editor/ThirdParty/imgui/imgui_widgets.cpp" "/home/federico/labiagi_project/build/srrg2_config_visualizer/imgui-node-editor/ThirdParty/imgui/CMakeFiles/imgui.dir/imgui_widgets.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ImDrawIdx=unsigned int"
  "ROS_BUILD_SHARED_LIBS=1"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/federico/labiagi_project/src/srrg2_config_visualizer/src/third_party/imgui-node-editor/ThirdParty/imgui"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
