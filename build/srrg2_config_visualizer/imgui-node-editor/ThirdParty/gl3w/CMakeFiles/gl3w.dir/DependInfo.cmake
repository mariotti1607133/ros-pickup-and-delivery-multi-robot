# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/federico/labiagi_project/src/srrg2_config_visualizer/src/third_party/imgui-node-editor/ThirdParty/gl3w/Source/gl3w.c" "/home/federico/labiagi_project/build/srrg2_config_visualizer/imgui-node-editor/ThirdParty/gl3w/CMakeFiles/gl3w.dir/Source/gl3w.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "ROS_BUILD_SHARED_LIBS=1"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/federico/labiagi_project/src/srrg2_config_visualizer/src"
  "/usr/include/eigen3"
  "/home/federico/labiagi_project/src/srrg_cmake_modules/cmake_modules"
  "/home/federico/labiagi_project/src/srrg2_core/srrg2_core/src"
  "/opt/ros/melodic/include"
  "/usr/include/opencv"
  "/home/federico/labiagi_project/src/srrg2_config_visualizer/src/third_party/imgui-node-editor/ThirdParty/gl3w/Include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
