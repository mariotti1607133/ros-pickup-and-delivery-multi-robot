# CMake generated Testfile for 
# Source directory: /home/federico/labiagi_project/src/srrg2_config_visualizer/src/third_party/imgui-node-editor
# Build directory: /home/federico/labiagi_project/build/srrg2_config_visualizer/src/third_party/imgui-node-editor
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("Common/Application")
subdirs("Common/BlueprintUtilities")
