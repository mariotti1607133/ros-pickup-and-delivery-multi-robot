# CMake generated Testfile for 
# Source directory: /home/federico/labiagi_project/src/srrg2_config_visualizer/src
# Build directory: /home/federico/labiagi_project/build/srrg2_config_visualizer/src
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("app")
subdirs("srrg_config_visualizer")
subdirs("third_party")
