# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/federico/labiagi_project/src/srrg2_config_visualizer/src/app/app_node_editor.cpp" "/home/federico/labiagi_project/build/srrg2_config_visualizer/src/app/CMakeFiles/app_node_editor.dir/app_node_editor.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ImDrawIdx=unsigned int"
  "ROS_BUILD_SHARED_LIBS=1"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/federico/labiagi_project/src/srrg2_config_visualizer/src"
  "/usr/include/eigen3"
  "/home/federico/labiagi_project/src/srrg_cmake_modules/cmake_modules"
  "/home/federico/labiagi_project/src/srrg2_core/srrg2_core/src"
  "/opt/ros/melodic/include"
  "/usr/include/opencv"
  "/home/federico/labiagi_project/src/srrg2_config_visualizer/src/third_party/imgui-node-editor/Common/Application/Include"
  "/home/federico/labiagi_project/src/srrg2_config_visualizer/src/third_party/imgui-node-editor/Common/BlueprintUtilities/Include"
  "/home/federico/labiagi_project/src/srrg2_config_visualizer/src/third_party/imgui-node-editor/ThirdParty/imgui"
  "/home/federico/labiagi_project/src/srrg2_config_visualizer/src/third_party/imgui-node-editor/ThirdParty/gl3w"
  "/home/federico/labiagi_project/src/srrg2_config_visualizer/src/third_party/imgui-node-editor/NodeEditor/Include"
  "/home/federico/labiagi_project/src/srrg2_config_visualizer/src/third_party/imgui-node-editor/NodeEditor/Source"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/federico/labiagi_project/build/srrg2_config_visualizer/imgui-node-editor/ThirdParty/imgui/CMakeFiles/imgui.dir/DependInfo.cmake"
  "/home/federico/labiagi_project/build/srrg2_config_visualizer/imgui-node-editor/NodeEditor/CMakeFiles/imgui_node_editor.dir/DependInfo.cmake"
  "/home/federico/labiagi_project/build/srrg2_config_visualizer/src/srrg_config_visualizer/CMakeFiles/srrg_config_visualizer_library.dir/DependInfo.cmake"
  "/home/federico/labiagi_project/build/srrg2_config_visualizer/src/third_party/imgui-node-editor/Common/Application/CMakeFiles/Application.dir/DependInfo.cmake"
  "/home/federico/labiagi_project/build/srrg2_config_visualizer/src/third_party/imgui-node-editor/Common/BlueprintUtilities/CMakeFiles/blueprint-utilities.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
