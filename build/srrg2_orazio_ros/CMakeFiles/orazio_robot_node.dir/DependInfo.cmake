# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/federico/labiagi_project/src/srrg2_orazio/srrg2_orazio_ros/src/orazio_robot_node.cpp" "/home/federico/labiagi_project/build/srrg2_orazio_ros/CMakeFiles/orazio_robot_node.dir/src/orazio_robot_node.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "HAVE_YAMLCPP_GT_0_5_0"
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"srrg2_orazio_ros\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/federico/labiagi_project/src/srrg_cmake_modules/cmake_modules"
  "/home/federico/labiagi_project/src/srrg2_orazio/srrg2_orazio/src/orazio_host"
  "/home/federico/labiagi_project/src/srrg2_orazio/srrg2_orazio/src/common"
  "/opt/ros/melodic/include"
  "/opt/ros/melodic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  "/usr/include/eigen3"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
