# CMake generated Testfile for 
# Source directory: /home/federico/labiagi_project/src/srrg2_qgl_viewport/srrg2_qgl_viewport_ros/src
# Build directory: /home/federico/labiagi_project/build/srrg2_qgl_viewport_ros/src
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("srrg_qgl_viewport_ros")
subdirs("tests")
subdirs("app")
