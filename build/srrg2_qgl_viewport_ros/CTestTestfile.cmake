# CMake generated Testfile for 
# Source directory: /home/federico/labiagi_project/src/srrg2_qgl_viewport/srrg2_qgl_viewport_ros
# Build directory: /home/federico/labiagi_project/build/srrg2_qgl_viewport_ros
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("src")
