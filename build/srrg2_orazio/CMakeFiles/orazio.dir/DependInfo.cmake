# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/federico/labiagi_project/src/srrg2_orazio/srrg2_orazio/src/orazio_host/linenoise.c" "/home/federico/labiagi_project/build/srrg2_orazio/CMakeFiles/orazio.dir/src/orazio_host/linenoise.c.o"
  "/home/federico/labiagi_project/src/srrg2_orazio/srrg2_orazio/src/orazio_host/orazio.c" "/home/federico/labiagi_project/build/srrg2_orazio/CMakeFiles/orazio.dir/src/orazio_host/orazio.c.o"
  "/home/federico/labiagi_project/src/srrg2_orazio/srrg2_orazio/src/orazio_host/orazio_shell_commands.c" "/home/federico/labiagi_project/build/srrg2_orazio/CMakeFiles/orazio.dir/src/orazio_host/orazio_shell_commands.c.o"
  "/home/federico/labiagi_project/src/srrg2_orazio/srrg2_orazio/src/orazio_host/orazio_shell_globals.c" "/home/federico/labiagi_project/build/srrg2_orazio/CMakeFiles/orazio.dir/src/orazio_host/orazio_shell_globals.c.o"
  "/home/federico/labiagi_project/src/srrg2_orazio/srrg2_orazio/src/orazio_host/orazio_ws_server.c" "/home/federico/labiagi_project/build/srrg2_orazio/CMakeFiles/orazio.dir/src/orazio_host/orazio_ws_server.c.o"
  "/home/federico/labiagi_project/src/srrg2_orazio/srrg2_orazio/src/orazio_host/ventry.c" "/home/federico/labiagi_project/build/srrg2_orazio/CMakeFiles/orazio.dir/src/orazio_host/ventry.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "ROS_BUILD_SHARED_LIBS=1"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/federico/labiagi_project/src/srrg_cmake_modules/cmake_modules"
  "/home/federico/labiagi_project/src/srrg2_orazio/srrg2_orazio/src/common"
  "/home/federico/labiagi_project/src/srrg2_orazio/srrg2_orazio/src/orazio_host"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/federico/labiagi_project/build/srrg2_orazio/CMakeFiles/srrg2_orazio_library.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
